using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Dynamic particle.
/// 
/// The dynamic particle is the backbone of the liquids effect. Its a circle with physics with 3 states, each state change its physic properties and its sprite color ( so the shader can separate wich particle is it to draw)
/// The particles scale down and die, and have a scale  effect towards their velocity.
/// 
/// Visit: www.codeartist.mx for more stuff. Thanks for checking out this example.
/// Credit: Rodrigo Fernandez Diaz
/// Contact: q_layer@hotmail.com
/// </summary>

public class DynamicParticle : MonoBehaviour {	
	public enum STATES{WATER,GAS,LAVA,INK,INACTIVE,NONE}; //The 3 states of the particle
	STATES currentState=STATES.NONE; //Defines the currentstate of the particle, default is water
	public GameObject currentImage; //The image is for the metaball shader for the effect, it is onle seen by the liquids camera.
	public GameObject[] particleImages; //We need multiple particle images to reduce drawcalls
	float GAS_FLOATABILITY=7.0f; //How fast does the gas goes up?
	float particleLifeTime=3.0f,startTime, currentTime = 0;//How much time before the particle scalesdown and dies	
    [SerializeField]
    private GameObject InkFloor;
    private List<bool> inkstack = new List<bool>();
    private Rigidbody2D myRigidbody2D;
    private bool sendSignal = false;
    private bool remove = false;
    Vector2 particleScale = new Vector2(2, 3);
    void Awake(){ 
		if (currentState == STATES.NONE)
			SetState (STATES.WATER);

        myRigidbody2D = GetComponent<Rigidbody2D>();
	}
    [SerializeField] private GameObject particleEffectInkSplash;

	//The definitios to each state
	public void SetState(STATES newState){
		if(newState!=currentState){ //Only change to a different state
			switch(newState){
				case STATES.WATER:													
					GetComponent<Rigidbody2D>().gravityScale=1.0f; // To simulate Water density
				break;
				case STATES.GAS:		
					particleLifeTime=particleLifeTime/2.0f;	// Gas lives the time the other particles
					GetComponent<Rigidbody2D>().gravityScale=0.0f;// To simulate Gas density
					gameObject.layer=LayerMask.NameToLayer("Gas");// To have a different collision layer than the other particles (so gas doesnt rises up the lava but still collides with the wolrd)
				break;					
				case STATES.LAVA:
					GetComponent<Rigidbody2D>().gravityScale=0.3f; // To simulate the lava density
				break;
                case STATES.INK:
                    GetComponent<CircleCollider2D>().isTrigger = true;
                    GetComponent<Rigidbody2D>().gravityScale = 0.1f; // To simulate the lava density
                    break;
                case STATES.NONE:
					Destroy(gameObject);
				break;
			}
			if(newState!=STATES.NONE){
				currentState=newState;
				startTime=Time.time;//Reset the life of the particle on a state change
				GetComponent<Rigidbody2D>().velocity=new Vector2();	// Reset the particle velocity	
				currentImage.SetActive(false);
				currentImage=particleImages[(int)currentState];
				currentImage.SetActive(true);
			}
		}		
	}
	void Update () {
		switch(currentState){
			case STATES.WATER: //Water and lava got the same behaviour
				MovementAnimation(); 
				ScaleDown();
			break;
			case STATES.LAVA:
				MovementAnimation();
				ScaleDown();
			break;
			case STATES.GAS:
				if(GetComponent<Rigidbody2D>().velocity.y<50){ //Limits the speed in Y to avoid reaching mach 7 in speed
					GetComponent<Rigidbody2D>().AddForce (new Vector2(0,GAS_FLOATABILITY)); // Gas always goes upwards
				}
				ScaleDown();
			break;
            case STATES.INK:
                if (remove)
                {
                    currentState = STATES.INACTIVE;
                }
                MovementAnimation();
                ScaleUp();
                SlowDown();
                
            break;
            case STATES.INACTIVE:
                currentTime = Time.time;
                GetComponent<CircleCollider2D>().isTrigger = true;
                if (transform.localScale.x <= transform.localScale.y)
                {
                    transform.localScale = new Vector3(transform.localScale.x + 0.03f, transform.localScale.y, transform.localScale.z);
                }
                if (Time.time - startTime >= particleLifeTime || remove)
                {
                    ScaleDown();
                }

                break;

        }	
	}
	// This scales the particle image acording to its velocity, so it looks like its deformable... but its not ;)
	void MovementAnimation(){
        Vector3 movementScale=new Vector3(1.0f,1.0f,1.0f);
        if ( myRigidbody2D != null)
        {
            movementScale.x += Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) / 40.0f;
            movementScale.z += Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y) / 40.0f;
        }	
		
		movementScale.y=1.0f;		
		currentImage.gameObject.transform.localScale=movementScale;
	}
	// The effect for the particle to seem to fade away
	void ScaleDown()
    {
        remove = true;
        if(remove && !sendSignal)
        {
            GetComponent<CircleCollider2D>().isTrigger = true;
            GetComponent<CircleCollider2D>().radius = 0.5f;
            sendSignal = true;
        }
		if (currentImage.GetComponent<MeshRenderer>().material.color.a <= 0)
        {
		    Destroy(gameObject);
		}
        else
        {
            currentImage.GetComponent<MeshRenderer>().material.color += new Color(0, 0, 0, -0.1f) * Time.deltaTime;
        }
	}
    void ScaleUp()
    {
        particleScale += Vector2.one * Time.deltaTime;
        transform.localScale = particleScale;
   
    }
    void SlowDown()
    {
        if (myRigidbody2D != null)
        {
            if (GetComponent<Rigidbody2D>().velocity.x < 1f && GetComponent<Rigidbody2D>().velocity.y < 1f && GetComponent<Rigidbody2D>().velocity.x > -1f && GetComponent<Rigidbody2D>().velocity.y > -1f)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                currentState = STATES.INACTIVE;
                //tag = "FloorInk";
                gameObject.layer = 11;
                //GetComponent<CircleCollider2D>().isTrigger = true;
            }
        }
    }
	// To change particles lifetime externally (like the particle generator)
	public void SetLifeTime(float time){
		particleLifeTime=time;	
	}
	// Here we handle the collision events with another particles, in this example water+lava= water-> gas
	void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player1") || coll.gameObject.CompareTag("Player2"))
        {
            coll.rigidbody.AddForce(coll.relativeVelocity * Time.deltaTime * 20000, ForceMode2D.Force);
        }

        if(coll.gameObject.tag=="DynamicParticle")
        {
            if(remove)
            {
                coll.gameObject.GetComponent<DynamicParticle>().SignalRemoval();
            }
            if (coll.gameObject.tag == "Arena Out")
            {
                Destroy(this);
            }
        }

	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "DynamicParticle")
        {
            if (remove)
            {
                other.GetComponent<DynamicParticle>().SignalRemoval();
            }
        }
        if (other.gameObject.tag == "Arena Out")
        {
            Destroy(this);
        }
	}

	void OnTriggerStay2D(Collider2D other)
    {
		if (other.name == "Leg 1 End" || other.name == "Leg 2 End" || other.name == "Leg 3 End" || other.name == "Leg 4 End" || other.name == "Leg 5 End" || other.name == "Leg 6 End" || other.name == "Leg 7 End")
		{
			other.GetComponent<Slip> ().fastened = false;
		}
        if (other.CompareTag("Player1") || other.CompareTag("Player2"))
        {
            other.attachedRigidbody.velocity += other.attachedRigidbody.velocity * Time.deltaTime * 0.5f;
        }
	}

    public void SignalRemoval()
    {
        GetComponent<CircleCollider2D>().isTrigger = true;
        GetComponent<CircleCollider2D>().radius = 0.5f;
        remove = true;
    }
}

