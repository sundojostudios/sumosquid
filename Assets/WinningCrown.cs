﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.ImageEffects
{
    public class WinningCrown : MonoBehaviour {

        [SerializeField] Transform moveto;
        [SerializeField] Transform camera;
        [SerializeField] AudioSource clapping;
        [SerializeField] AudioSource cheering;

        void Start()
        {
            camera.GetComponent<BloomAndFlares>().bloomIntensity = 0;
            camera.GetComponent<BloomAndFlares>().enabled = true;
            camera.GetComponent<BloomAndFlares>().bloomIntensity = 0;
            clapping.Play();
            cheering.Play();
        }

        void Update()
        {
            foreach (AudioSource source in Object.FindObjectsOfType<AudioSource>())
            {
                if (source.name == "Music Source")
                {
                    Destroy(source.gameObject); 
                }
            }

            transform.position = Vector3.MoveTowards(transform.position, moveto.position, 0.5f);
            transform.rotation = moveto.rotation;
            if(camera.GetComponent<BloomAndFlares>().bloomIntensity <= 1)
            {
                camera.GetComponent<BloomAndFlares>().bloomIntensity += 0.05f;
            }
            if (Vector3.Distance(transform.position, moveto.position) <= 0.05f)
            {
                transform.position = moveto.position;
            }
            
        }
    }
}
