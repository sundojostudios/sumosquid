﻿using UnityEngine;
using System.Collections;

public class setwinnerstyle : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer player1Eye1;
    [SerializeField]
    private SpriteRenderer player1Eye2;
    [SerializeField]
    private SpriteRenderer player1Eyelid1;
    [SerializeField]
    private SpriteRenderer player1Eyelid2;
    [SerializeField]
    private SpriteRenderer player1Mouth;
    [SerializeField]
    private SkinnedMeshRenderer[] player1Tentacles;
    [SerializeField]
    private SpriteRenderer player1Head;
    [Header("The order is: blue, yellow, pink, green.")]
    [Header("It goes through the colors for one style, and then the same for the next.")]
    [SerializeField]
    private Sprite[] eyes;
    [SerializeField]
    private Sprite[] eyelids;
    [SerializeField]
    private Sprite[] mouths;
    [SerializeField]
    private Sprite[] tentacles;
    [SerializeField]
    private Sprite[] heads;
    [Header("The order is: blue, yellow, pink, green.")]
    [Header("One style for all.")]
    [SerializeField]
    private Sprite[] woundedEyelids;
    [SerializeField]
    private Sprite[] woundedMouths;
    [SerializeField]
    private Sprite[] woundedHeads;
    void Start ()
    {
	    if (PreserveData.winsPlayer1 == 2)
        {
            player1Eye2.sprite = player1Eye1.sprite = eyes[(int)PreserveData.player1Style.eyes * 4 + (int)PreserveData.player1Style.color];
            player1Eyelid2.sprite = player1Eyelid1.sprite = eyelids[(int)PreserveData.player1Style.eyelids * 4 + (int)PreserveData.player1Style.color];
            player1Mouth.sprite = mouths[(int)PreserveData.player1Style.mouth * 4 + (int)PreserveData.player1Style.color];
            foreach (SkinnedMeshRenderer tentacle in player1Tentacles)
            {
                tentacle.material.mainTexture = tentacles[(int)PreserveData.player1Style.tentacles * 4 + (int)PreserveData.player1Style.color].texture;
            }
            player1Head.sprite = heads[(int)PreserveData.player1Style.color];
        }
        if(PreserveData.winsPlayer2 == 2)
        {
            player1Eye2.sprite = player1Eye1.sprite = eyes[(int)PreserveData.player2Style.eyes * 4 + (int)PreserveData.player2Style.color];
            player1Eyelid2.sprite = player1Eyelid1.sprite = eyelids[(int)PreserveData.player2Style.eyelids * 4 + (int)PreserveData.player2Style.color];
            player1Mouth.sprite = mouths[(int)PreserveData.player2Style.mouth * 4 + (int)PreserveData.player2Style.color];
            foreach (SkinnedMeshRenderer tentacle in player1Tentacles)
            {
                tentacle.material.mainTexture = tentacles[(int)PreserveData.player2Style.tentacles * 4 + (int)PreserveData.player2Style.color].texture;
            }
            player1Head.sprite = heads[(int)PreserveData.player2Style.color];
        }
	}
}
