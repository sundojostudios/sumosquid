﻿using UnityEngine;
using System.Collections;

public class TutorialRemoval : MonoBehaviour {

    [SerializeField]
    Transform warmupTutorialObj;
	
	void Update ()
    {
	    if (!warmupTutorialObj.gameObject.active)
        {
            this.gameObject.SetActive(false);
        }
	}
}
