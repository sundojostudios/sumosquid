﻿using UnityEngine;
using System.Collections;

public class Announcer321 : MonoBehaviour {

    [SerializeField] public AudioSource Source321;
    [SerializeField]
    public AudioSource SourceFinal;
    [SerializeField]
    public AudioSource SourceTie;
    private bool playedBool = false;
    private bool playedBoolFinal = false;
    private bool playedBoolBreaker = false;

    // Update is called once per frame
    void Update ()
    {
	    if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("321") && !playedBool)
        {
            Source321.Play();
            //Source321.pitch = 1.2f;
            playedBool = true;
        }
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("finalround") && !playedBoolFinal)
        {
            SourceFinal.Play();
            //Source321.pitch = 1.2f;
            playedBoolFinal = true;
        }
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Tiebreaker") && !playedBoolBreaker)
        {
            SourceTie.Play();
            //Source321.pitch = 1.2f;
            playedBoolBreaker = true;
        }
    }
}
