﻿using UnityEngine;
using System.Collections;

public class scaling : MonoBehaviour {



	private bool shrinking;
	[SerializeField]
	[Range(0.0001f, 0.1f)]
	private float Speed = 0.005f;
	public float ScalingSpeed
	{
		get{return Speed;}
		set{Speed = value;}
	}

	[SerializeField]
	[Range(0.05f, 50.0f)]
	private float MinScale = 0.05f;
	public float ScalingMin
	{
		get { return MinScale; }
		set { MinScale = value; }
	}

	[SerializeField]
	[Range(0.05f, 50.0f)]
	private float MaxScale;
	public float ScalingMax
	{
		get { return MaxScale; }
		set { MaxScale = value; }
	}

	void Update ()
	{

		if(!shrinking)
		{
			transform.localScale += new Vector3(Speed, 0, 0);
		}

		if (!shrinking)
		{
			transform.localScale += new Vector3(0, Speed, 0);
		}

		if (shrinking)
		{
			transform.localScale -= new Vector3(Speed, 0, 0);
		}

		if (shrinking)
		{
			transform.localScale -= new Vector3(0, Speed, 0);
		}


		if (transform.localScale.y >= MaxScale && transform.localScale.y >= MaxScale)
		{
			shrinking = true;
		}
		else if(transform.localScale.y <= MinScale && transform.localScale.x <= MinScale)
		{
			shrinking = false;
		}
	}

}