﻿using UnityEngine;
using System.Collections;

public class Done_BGScroller : MonoBehaviour
{
	public float scrollSpeed;
	public float tileSizeZ;
	public bool goingDown = false; 
	private Vector3 startPosition;

	void Start ()
	{
		startPosition = transform.position;
	}

	void Update ()
	{
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeZ);
		if (!goingDown) {
			transform.position = startPosition + Vector3.up * newPosition;
		} else {
			transform.position = startPosition + Vector3.down * newPosition;
		}
	}
}