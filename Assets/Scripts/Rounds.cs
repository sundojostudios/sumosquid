﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Rounds : MonoBehaviour
{
	[SerializeField] private Text textRound;
	[SerializeField] private Text textWins;
	[SerializeField] private Transform player1;
	[SerializeField] private Transform player2;
	private float showTextCountDown;
	[SerializeField] private float showTextTime;
	[SerializeField] private Transform rematchMenu;
	private bool afermatch = false;

	void Awake()
	{
		showTextCountDown = showTextTime;
		if (PreserveData.round == 0)
		{
			PreserveData.winsPlayer1 = PreserveData.winsPlayer2 = 0;
		}
	}

	void Update ()
	{
		if (afermatch) 
		{
			PreserveData.round = 0;
			rematchMenu.gameObject.SetActive(true);
			Camera.main.transform.position = rematchMenu.position - Vector3.forward * 10;
			Camera.main.orthographicSize = 14;
		}
		else if (player1 == null || player2 == null)
		{
			if(showTextCountDown >= 0)
			{
				showTextCountDown -= Time.deltaTime;
				return;
			}
			showTextCountDown = showTextTime;

			if (GetComponent<Winning>().winner == Winning.TheWinner.PLAYER1)
			{
				PreserveData.winsPlayer1++;
				PreserveData.player1Style.wounded = false;
				PreserveData.player2Style.wounded = true;
			}
			if (GetComponent<Winning>().winner == Winning.TheWinner.PLAYER2)
			{
				PreserveData.winsPlayer2++;
				PreserveData.player1Style.wounded = true;
				PreserveData.player2Style.wounded = false;
			}
			if (GetComponent<Winning> ().winner == Winning.TheWinner.BOTH)
            {
                
                if (PreserveData.winsPlayer1 == 0 && PreserveData.winsPlayer2 == 0)
                {
                    PreserveData.round--;
                }
                PreserveData.player1Style.wounded = true;
				PreserveData.player2Style.wounded = true;
			}



            if (PreserveData.winsPlayer1 >= 2 || PreserveData.winsPlayer2 >= 2)
			{
				afermatch = true;
				GetComponent<Winning>().afermatch = true;
				if (player1 != null)
				{
					Destroy(player1.gameObject);
				}
				else if (player2 != null)
				{
					Destroy(player2.gameObject);
				}
			}
			else
			{
				PreserveData.round++;
				Loading.LoadScene(SceneManager.GetActiveScene().name);
			}
		}
	}
	
	void OnGUI ()
	{
		textRound.text = new string[]{ "First Round", "Second Round", "Tie Breaker" }[Mathf.Clamp(PreserveData.round, 0, 2)];
		textWins.text = PreserveData.winsPlayer1 + " : " + PreserveData.winsPlayer2;
	}
}
