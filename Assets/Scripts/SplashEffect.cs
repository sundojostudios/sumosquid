﻿using UnityEngine;
using System.Collections;

public class SplashEffect : MonoBehaviour {
    [SerializeField] Transform player1Body;
    [SerializeField] GameObject particleEffect;
    [SerializeField]
    bool useParticleFX = true;
    bool playerPlayed = false;

    Vector3 playerPosition;

	void Update ()
    {
        if(player1Body != null)
        {
            playerPosition = player1Body.position;
        }
        else
        {
            if (PreserveData.winsPlayer1 < 2 && PreserveData.winsPlayer2 < 2)
            {
                if (!playerPlayed)
                {
                    if (useParticleFX)
                    {
                        GameObject TempObj = Instantiate(particleEffect, playerPosition + Vector3.forward * 5, Quaternion.identity) as GameObject;
                    }
                    playerPlayed = true;
                    GetComponent<AudioSource>().pitch = Random.Range(0.8f, 1.3f);
                    GetComponent<AudioSource>().Play();
                }
            }
        }
    }
}
