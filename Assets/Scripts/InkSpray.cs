﻿using UnityEngine;
using System.Collections;

public class InkSpray : MonoBehaviour
{
	[SerializeField] private GameObject blob;
	private float blobCountdown;
	[SerializeField] private float frequency;
	[SerializeField] private Transform otherPlayerBody;
	[HideInInspector] public string inkSprayButton;
	/*[HideInInspector]*/ public float filledInk = 0;
	[SerializeField] private float depletionRate = 100;
    [SerializeField] private ParticleSystem inkParticleSystem;
    [SerializeField] private ParticleGenerator inkShooting;
	[SerializeField] private SpriteRenderer sprite1;
	[SerializeField] private SpriteRenderer sprite2;
	[SerializeField] private SpriteRenderer sprite3;
    [SerializeField]
    private AudioSource shootingSound;
    [SerializeField]
    private AudioSource shootingSound2;
    [SerializeField] private Sprite filledInkSprite1;
	[SerializeField] private Sprite filledInkSprite2;
	[SerializeField] private Sprite filledInkSprite3;

    void Update () {
		filledInk = Mathf.Clamp(filledInk, 0, 40);

		sprite1.color = new Color(1, 1, 1, Mathf.Clamp((filledInk / 40) * 3, 0, 1));
		sprite2.color = new Color(1, 1, 1, Mathf.Clamp((filledInk / 40) * 2, 0, 1));
        sprite3.color = new Color(1, 1, 1, Mathf.Clamp((filledInk / 40), 0, 1));
        
        if ((SumoInput.GetButton(inkSprayButton, transform) || GetComponent<MicrophoneInk>().Shoot() || SumoInput.GetAxis(inkSprayButton, transform) < -0.1f) && filledInk > 0)
		{
            inkParticleSystem.Play();
            if(!shootingSound.isPlaying)
            {
                shootingSound.Play();
                shootingSound2.Play();
            }
            inkShooting.enabled = true;
            filledInk -= Time.deltaTime * depletionRate;
			if (blobCountdown >= 0)
			{
				blobCountdown -= Time.deltaTime;
			}
			else
			{
                //GameObject blobInstance = (GameObject)Instantiate(blob, transform.position, Quaternion.Euler((transform.position - otherPlayerBody.position).normalized));
				//blobInstance.tag = tag;
				//blobInstance.GetComponent<Rigidbody2D>().AddForce((otherPlayerBody.position - transform.position).normalized * 50, ForceMode2D.Impulse);
				//blobInstance.layer = gameObject.layer;
				//blobInstance.tag = gameObject.tag;
				blobCountdown = frequency;
			}
		}
        else
        {
            inkShooting.enabled = false;
            shootingSound.Stop();
            shootingSound2.Stop();
            inkParticleSystem.Stop();
        }
        if(SumoInput.GetButtonUp(inkSprayButton, transform) || SumoInput.GetAxis(inkSprayButton, transform) <= 0)
		{
			blobCountdown = 0;
		}
	}
}
