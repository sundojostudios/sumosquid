﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundLights : MonoBehaviour {
    [SerializeField]
    RectTransform Player1Round1;
    [SerializeField]
    RectTransform Player1Round2;
    [SerializeField]
    RectTransform Player2Round1;
    [SerializeField]
    RectTransform Player2Round2;
    [SerializeField]
    Sprite LitSprite;
    void Start ()
    {
	    if (PreserveData.winsPlayer1 == 1)
        {
            Player1Round1.GetComponent<Image>().sprite = LitSprite;
        }
        
        if (PreserveData.winsPlayer2 == 1)
        {
            Player2Round1.GetComponent<Image>().sprite = LitSprite;
        }
        if (PreserveData.winsPlayer2 == 2)
        {
            Player2Round2.GetComponent<Image>().sprite = LitSprite;
        }
        if (PreserveData.winsPlayer1 == 2)
        {
            Player1Round2.GetComponent<Image>().sprite = LitSprite;
        }
    }
    void Update()
    {
        if (PreserveData.winsPlayer2 == 2)
        {
            Player2Round2.GetComponent<Image>().sprite = LitSprite;
        }
        if (PreserveData.winsPlayer1 == 2)
        {
            Player1Round2.GetComponent<Image>().sprite = LitSprite;
        }
    }
    public void Light(int winlamp)
    {
        if (winlamp == 11)
        {
            Player1Round1.GetComponent<Image>().sprite = LitSprite;
        }

        if (winlamp == 21)
        {
            Player2Round1.GetComponent<Image>().sprite = LitSprite;
        }
        if (winlamp == 22)
        {
            Player2Round2.GetComponent<Image>().sprite = LitSprite;
        }
        if (winlamp == 12)
        {
            Player1Round2.GetComponent<Image>().sprite = LitSprite;
        }
    }
}
