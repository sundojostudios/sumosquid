﻿using UnityEngine;
using System.Collections;

public class RoundWinner : MonoBehaviour
{
    private bool played = false;

    [SerializeField]
    Transform p1Round1;
    [SerializeField]
    Transform p1Round2;
    [SerializeField]
    Transform p2Round1;
    [SerializeField]
    Transform p2Round2;
    [SerializeField]
    Transform lightHandeler;
    [SerializeField]
    Transform fizzle;

    [SerializeField]
    GameObject explosionFX;

    [HideInInspector]
    public bool p1win = false;
    [HideInInspector]
    public bool p2win = false;

    RectTransform myTransform;
    void Start()
    {
        myTransform = GetComponent<RectTransform>();
    }
    void Update()
    {
        if(p1win)
        {
            Player1();
        }
        if (p2win)
        {
            Player2();
        }
        if(myTransform.position == p1Round1.position || myTransform.position == p1Round2.position || myTransform.position == p2Round1.position || myTransform.position == p2Round2.position)
        {
            fizzle.gameObject.SetActive(false);
            GameObject explosion = (GameObject)Instantiate(explosionFX,myTransform.position, myTransform.localRotation);
            if(myTransform.position == p1Round1.position)
            {
                lightHandeler.GetComponent<RoundLights>().Light(11);
                explosion.transform.SetParent(p1Round1);
            }
            if (myTransform.position == p1Round2.position)
            {
                lightHandeler.GetComponent<RoundLights>().Light(12);
                explosion.transform.SetParent(p1Round2);
            }
            if (myTransform.position == p2Round1.position)
            {
                lightHandeler.GetComponent<RoundLights>().Light(21);
                explosion.transform.SetParent(p2Round1);
            }
            if (myTransform.position == p2Round2.position)
            {
                lightHandeler.GetComponent<RoundLights>().Light(22);
                explosion.transform.SetParent(p2Round2);
            }
           
            this.gameObject.SetActive(false);
        }
    }
    public void Player1()
    {

        if (!played)
        {
            GetComponent<Animator>().Play("p1win");
            played = true;
        }
        if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("fireball") && PreserveData.winsPlayer1 == 0)
        {
            fizzle.gameObject.SetActive(true);
           myTransform.position = Vector3.MoveTowards(transform.position, p1Round1.position, 2f);
        }
        else if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("fireball") && PreserveData.winsPlayer1 == 1)
        {
            fizzle.gameObject.SetActive(true);
            myTransform.position = Vector3.MoveTowards(transform.position, p1Round2.position, 2f);
        }
    }
    public void Player2()
    {
        if (!played)
        {
            GetComponent<Animator>().Play("p2win");
            played = true;
        }
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("fireball") && PreserveData.winsPlayer2 == 0)
        {
            fizzle.gameObject.SetActive(true);
            myTransform.position = Vector3.MoveTowards(transform.position, p2Round1.position, 2f);
        }
        else if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("fireball") && PreserveData.winsPlayer2 == 1)
        {
            fizzle.gameObject.SetActive(true);
            myTransform.position = Vector3.MoveTowards(transform.position, p2Round2.position, 2f);
        }
    }
    public void Tie()
    {
        if (!played)
        {
            GetComponent<Animator>().Play("tie");
            played = true;
        }

    }
    void OnDisable()
    {
        //explosion.gameObject.SetActive(true);
    }
}
