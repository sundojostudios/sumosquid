﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour {

    [SerializeField] public Transform target;
    [SerializeField] private string horizontalInput;
	[SerializeField] private string verticalInput;
	[SerializeField] private string horizontalInputB;
	[SerializeField] private string verticalInputB;
	[SerializeField] private float rotateTowardsSpeed = 0;

	Quaternion LookAt2D(Vector3 position, Vector3 targetPosition, int axis = 2)
    {
        Vector3 diff = targetPosition - position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		if (axis == 0)
		{
			return Quaternion.Euler(rot_z - 90, 0f, 0f);
		}
		else if (axis == 1)
		{
			return Quaternion.Euler(0f, rot_z - 90, 0f);
		}
		else
		{
			return Quaternion.Euler(0f, 0f, rot_z - 90);
		}
    }

    void Update ()
    {
        if (target != null)
        {
            if (transform.gameObject.name == "Body") 
			{
                if (!GetComponent<SelectLegAndLaunch>().isInSLingshot)
                {
                    transform.rotation = LookAt2D(transform.position, target.position);
                }
                else
                {
                    transform.rotation = LookAt2D(Vector3.zero, new Vector3(SumoInput.GetAxis(horizontalInput, transform) * -1 + SumoInput.GetAxis(horizontalInputB, transform) * -1, SumoInput.GetAxis(verticalInput, transform) * -1 + SumoInput.GetAxis(verticalInputB, transform) * -1, transform.localPosition.z));

                }
            } 
			else 
			{
				transform.rotation = Quaternion.RotateTowards(transform.rotation, LookAt2D(transform.position, target.position), rotateTowardsSpeed == 0 ? 1000000000 : Time.deltaTime * rotateTowardsSpeed);
			}
        }
    }
}
