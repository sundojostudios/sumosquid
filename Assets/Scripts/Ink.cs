﻿using UnityEngine;
using System.Collections;

public class Ink : MonoBehaviour {

	[SerializeField] private ParticleSystem inkParticleSystem;

	void Update () {
		//transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime + Mathf.Sin(Time.time * 10) * 0.02f, transform.localScale.y - Time.deltaTime + Mathf.Sin(Time.time * 10) * 0.02f, 1);
		transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime, transform.localScale.y - Time.deltaTime, 1);
		if (transform.localScale.x <= 0)
		{
			Destroy(gameObject);
		}
	}


	void OnTriggerEnter2D(Collider2D coll)
	{
		inkParticleSystem.Play();
	}
}
