﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

enum MenuTypes
{
    SINGLE_CHOICE, MULTI_CHOICE, MULTI_CHOICE_SELECT_ONE
}

public class MenuHandeler : MonoBehaviour
{
    [SerializeField]
    Transform[] menuOptions;

    [SerializeField]
    string[] optionScenes;

    [SerializeField] private CircleMenuObjNavig selectPlayer1;
    [SerializeField] private CircleMenuObjNavig selectPlayer2;

    bool sceneChange = false;
    bool slotMode = false;
    bool cancelInput = false;
    bool userand = false;
    public bool pubSlot
    {
        get { return slotMode; }
    }

    int player1Selected = -1;
    int player2Selected = -1;
    int rand = -1;
    float startTime;

	[SerializeField] private Transform current;
	[SerializeField] private Transform next;
    [SerializeField] private MenuTypes menuType;

    [SerializeField] public Transform infoPlayer1;
    [SerializeField] public Transform infoPlayer2;

    void Start ()
    {
        int nr = 0;
        foreach (Transform option in menuOptions)
        {
            option.GetComponent<MenuOption>().setOptionNr(nr);
            nr++;
        }

        PreserveData.warmup = true;
	}
	
	void Update ()
    {
        if(menuType == MenuTypes.SINGLE_CHOICE)
        {
            SingleChoice();
        }
        if (menuType == MenuTypes.MULTI_CHOICE)
        {
            MultipleChoice();
        }
        if (menuType == MenuTypes.MULTI_CHOICE_SELECT_ONE)
        {
            MultipleChoiceSelectOne();
        }
    }

    void SingleChoice()
    {
        if (!slotMode)
        {
            if (player1Selected != -1 || player2Selected != -1)
            {
                cancelInput = true;
                slotMode = true;
                startTime = Time.time;
                StartCoroutine(SingleBlink(2));
            }
        }
        if (sceneChange)
        {
            ChangeScene();
        }
    }

    void MultipleChoice()
    {
        if (!slotMode)
        {
            if (player1Selected != -1)
            {
                menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.red;
                selectPlayer1.enabled = false;
            }
            if (player2Selected != -1)
            {
                menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.red;
                selectPlayer2.enabled = false;
            }
            if (player1Selected != -1 && player2Selected != -1)
            {
                cancelInput = true;
                slotMode = true;
                startTime = Time.time;
                StartCoroutine(MultipleBlink(2, false));
            }
        }
        if (sceneChange)
        {
            ChangeScene();
        }
    }
    void MultipleChoiceSelectOne()
    {
        if (!slotMode)
        {
            if (player1Selected != -1)
            {
                menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.red;
            }
            if (player2Selected != -1)
            {
                menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.red;
            }
            if (player1Selected != -1 && player2Selected != -1)
            {
                cancelInput = true;
                slotMode = true;
                userand = true;
                rand = (int)Random.Range(1.0f, 2.99f);
                startTime = Time.time;
                if (player1Selected != player2Selected)
                {
                    menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.white;
                    StartCoroutine(MultipleBlink(2, true));
                }
                else
                {
                    StartCoroutine(SingleBlink(2));
                }


            }
        }
        if (sceneChange)
        {
            ChangeScene();
        }
    }

    void ChangeScene()
    {
		if (next != null)
		{
			next.gameObject.SetActive (true);
			current.gameObject.SetActive (false);
			return;
		}

        if (player1Selected == player2Selected)
        {
            Loading.LoadScene(optionScenes[player1Selected]);
        }
        else
        {
            if (userand)
            {
                if (rand == 1)
                {
                    Loading.LoadScene(optionScenes[player1Selected]);
                }
                else if (rand == 2)
                {
                    Loading.LoadScene(optionScenes[player2Selected]);
                }
            }
            else
            {
                if (player1Selected != -1)
                {
                    Loading.LoadScene(optionScenes[player1Selected]);
                }
                else if (player2Selected != -1)
                {
                    Loading.LoadScene(optionScenes[player2Selected]);
                }
            }
            
        }
    }
    IEnumerator SingleBlink(float time)
    {
        while (time > Time.time - startTime)
        {
            if (player1Selected == player2Selected)
            {
                if (menuOptions[player1Selected].GetComponent<SpriteRenderer>().color == Color.red)
                {
                    menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.white;
                }
                else
                {
                    menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.red;
                }
            }
            else
            {
                if (userand)
                {
                    if (rand == 1)
                    {
                        if (player2Selected != -1)
                        {
                            menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }

                        if (menuOptions[player1Selected].GetComponent<SpriteRenderer>().color == Color.red)
                        {
                            menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        else
                        {
                            menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }
                    else
                    {
                        if (player1Selected != -1)
                        {
                            menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        if (menuOptions[player2Selected].GetComponent<SpriteRenderer>().color == Color.red)
                        {
                            menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        else
                        {
                            menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }
                }
                else
                {
                    if (player1Selected != -1)
                    {
                        if (player2Selected != -1)
                        {
                            menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }

                        if (menuOptions[player1Selected].GetComponent<SpriteRenderer>().color == Color.red)
                        {
                            menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        else
                        {
                            menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }
                    else
                    {
                        if (player1Selected != -1)
                        {
                            menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        if (menuOptions[player2Selected].GetComponent<SpriteRenderer>().color == Color.red)
                        {
                            menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        else
                        {
                            menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }
                }
               
            }
            yield return new WaitForSeconds(0.1f);
        }
        sceneChange = true;
    }
   
    IEnumerator MultipleBlink(float time, bool single)
    {
        while (time > Time.time - startTime)
        {
            if(menuOptions[player1Selected].GetComponent<SpriteRenderer>().color == Color.red)
            {
                menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.white;
            }
            else
            {
                menuOptions[player1Selected].GetComponent<SpriteRenderer>().color = Color.red;
            }
            if (menuOptions[player2Selected].GetComponent<SpriteRenderer>().color == Color.red)
            {
                menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.white;
            }
            else
            {
                menuOptions[player2Selected].GetComponent<SpriteRenderer>().color = Color.red;
            }
            yield return new WaitForSeconds(0.2f);
        }

        startTime = Time.time;
        if (single)
        {
            StartCoroutine(SingleBlink(1));
        }
        else
        {
            sceneChange = true;
        }
    }
    public void PlayerSelect(int player, int option)
	{
		if (cancelInput)
		{
			return;
		}
		if (player == 1)
		{
			if (option == player1Selected)
			{
				menuOptions[player1Selected].GetComponent<SpriteRenderer> ().color = Color.white;
                selectPlayer1.enabled = true;
				player1Selected = -1;
			}
			else
			{
				if (player1Selected != -1)
				{
                    menuOptions[player1Selected].GetComponent<SpriteRenderer> ().color = Color.white;
                    selectPlayer1.enabled = true;
				}
				player1Selected = option;
			}
		}
		if (player == 2)
		{
			if (option == player2Selected)
			{
                menuOptions[player2Selected].GetComponent<SpriteRenderer> ().color = Color.white;
                selectPlayer2.enabled = true;
				player2Selected = -1;
			}
			else
			{
				if (player2Selected != -1)
				{
                    menuOptions[player2Selected].GetComponent<SpriteRenderer> ().color = Color.white;
                    selectPlayer2.enabled = true;
				}
				player2Selected = option;
			}
		}
	}
}
