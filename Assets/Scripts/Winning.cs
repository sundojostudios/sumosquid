﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Winning : MonoBehaviour {

	public enum TheWinner
	{
		NONE,
		PLAYER1,
		PLAYER2,
		BOTH
	}

	[SerializeField] private Transform player1;
	[SerializeField] private Transform player2;
    [SerializeField] private Transform Announcer;
    [SerializeField] private Transform Music;
    [SerializeField] private Image panel;
	[SerializeField] private Text text;
    [SerializeField] private float tieTime = 1;
    private bool winningBool = true;
    [SerializeField]
    private Transform WinAnimation;
	private float startTime = 0;
	[HideInInspector] public TheWinner winner = TheWinner.NONE;
	[HideInInspector] public bool afermatch = false;
    //[HideInInspector] public bool  = false;
    public bool lowerMusicVolume;
    public bool musicBool
    {
        get { return lowerMusicVolume; }
    }

	void Update()
	{
		if (afermatch)
		{
			text.text = "";
			return;
		}

		if (player1 == null || player2 == null)
		{
			panel.transform.localScale += Time.deltaTime * 0.5f * Vector3.one;

			if (startTime == 0)
			{
				winner = player1 == null ? TheWinner.PLAYER2 : TheWinner.PLAYER1;
				startTime = Time.time;
				panel.gameObject.SetActive(true);
			}

			if (player1 == null && player2 == null)
			{
				if (Time.time - startTime >= tieTime && winningBool)
				{
					winner = TheWinner.BOTH;
					panel.gameObject.SetActive(true);
                    Announcer.gameObject.GetComponent<AnnouncerScript>().tieBool = true;
                    WinAnimation.gameObject.SetActive(true);
                    WinAnimation.GetComponent<RoundWinner>().Tie();
                    winningBool = false;
                }
			}
           
        }

        
        if (winner == TheWinner.PLAYER1 && winningBool && Time.time - startTime >= tieTime)
        {
            Announcer.gameObject.GetComponent<AnnouncerScript>().p1winBool = true;
            winningBool = false;
            WinAnimation.gameObject.SetActive(true);
            WinAnimation.GetComponent<RoundWinner>().p1win = true;
        }

        if (winner == TheWinner.PLAYER2 && winningBool && Time.time - startTime >= tieTime)
        {
                //  GetComponent<AnnouncerScript>().p2winBool = true;
                WinAnimation.gameObject.SetActive(true);
                WinAnimation.GetComponent<RoundWinner>().p2win = true;
            
            Announcer.gameObject.GetComponent<AnnouncerScript>().p2winBool = true;
            winningBool = false;
        }

        if(winner != TheWinner.NONE)
        {
            lowerMusicVolume = true;
        }
        else
        {
            lowerMusicVolume = false;
        }

        switch (winner)
		{
		case TheWinner.BOTH:
			text.text = "Tie!";
			break;
		case TheWinner.PLAYER1:
			text.text = "Player 1 wins!"; 
			break;
		case TheWinner.PLAYER2:
			text.text = "Player 2 wins!";
            break;
		}
	}
}
