﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SuctionCupFX : MonoBehaviour {

    [SerializeField] public AudioSource[] SuctionSource;
    private bool played = true;
    public bool setPlayBool
    {
        set { played = value; }
    }
    bool canBePlayed = true;
    public bool canPlay
    {
        set { canBePlayed = value; }
    }
    private int randomPlay;
  
    

    	
	void Update ()
    {

	    if(!played && !SuctionSource[randomPlay].isPlaying && canBePlayed)
        {
            
            randomPlay = Random.Range(0, 6);
            SuctionSource[randomPlay].volume = 0.4f;
            SuctionSource[randomPlay].pitch = Random.Range(0.6f, 1.5f);
            SuctionSource[randomPlay].Play();
            played = true;
            canBePlayed = false;
        }
       
	}
}
