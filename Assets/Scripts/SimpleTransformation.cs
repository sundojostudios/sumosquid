﻿using UnityEngine;
using System.Collections;

public class SimpleTransformation : MonoBehaviour {

	[System.Serializable] public struct Trans
	{
		public Vector3 moveSpeed;
		[Space(10)]
		[Range(-720, 720)] public float rotateSpeed;
		[Space(10)]
		[Range(-2, 2)] public float ResizeSpeed;
		[Space(10)]
		public Vector3 moveSineSpeed;
		public Vector3 moveSineSize;
		[Space(10)]
		public Vector3 moveCosSpeed;
		public Vector3 moveCosSize;
		[Space(10)]
		[Range(-10, 10)] public float rotateSineSpeed;
		[Range(-10, 10)] public float rotateSineSize;
		[Space(10)]
		[Range(-10, 10)] public float ResizeSineSpeed;
		[Range(-1, 1)] public float ResizeSineSize;
	}

	[SerializeField] private Trans[] transforms;

	void Update () {
		foreach(Trans trans in transforms)
		{
			transform.localPosition += trans.moveSpeed * Time.deltaTime;
			transform.localEulerAngles += new Vector3(0, 0, trans.rotateSpeed * Time.deltaTime);
			transform.localScale += Vector3.one * trans.ResizeSpeed * Time.deltaTime;
			transform.localPosition += new Vector3(Mathf.Sin(Time.time * trans.moveSineSpeed.x) * trans.moveSineSize.x, Mathf.Sin(Time.time * trans.moveSineSpeed.y) * trans.moveSineSize.y, Mathf.Sin(Time.time * trans.moveSineSpeed.z) * trans.moveSineSize.z);
			transform.localPosition += new Vector3(Mathf.Cos(Time.time * trans.moveCosSpeed.x) * trans.moveCosSize.x, Mathf.Cos(Time.time * trans.moveCosSpeed.y) * trans.moveCosSize.y, Mathf.Cos(Time.time * trans.moveCosSpeed.z) * trans.moveCosSize.z);
			transform.localEulerAngles += new Vector3(0, 0, Mathf.Sin(Time.time * trans.rotateSineSpeed) * trans.rotateSineSize);
			transform.localScale += Vector3.one * Mathf.Sin(Time.time * trans.ResizeSineSpeed) * trans.ResizeSineSize;
		}
	}
}
