﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Idle : MonoBehaviour {

    [SerializeField] private float time;
    [SerializeField] private string scene;
    private float counter = 0;
    [SerializeField] private string[] gamePadAxes;
    [SerializeField] private Text text;

	void Update () {
        if (time - counter <= 3)
        {
            text.text = "Returning to main menu in\n" + Mathf.Ceil(time - counter).ToString();
        }
        else
        {
            return;//text.text = "";
        }

        if(counter >= time)
        {
            Loading.LoadScene(scene);
        }

        if (Input.anyKey)
        {
            counter = 0;
        }

        foreach (string str in gamePadAxes)
        {
            if (SumoInput.GetAxis(str, transform) != 0)
            {
                counter = 0;
            }
        }

        counter += Time.deltaTime;
	}
}
