﻿using UnityEngine;
using System.Collections;

public class TutorialBox : MonoBehaviour
{
    [SerializeField]
    private GameObject playerBody;
    [SerializeField]
    private float offsetX = 0;
    [SerializeField]
    private float offsetY = 0;
    // Update is called once per frame
    void Update ()
    {
        transform.position = new Vector2(playerBody.transform.position.x + offsetX, playerBody.transform.position.y + offsetY);
	}
}
