﻿using UnityEngine;
using System.Collections;

public class SuctionPitch : MonoBehaviour {

    [SerializeField ]private AudioSource[] suctionSources;
    private int randomSource;
	
	// Update is called once per frame
	void Update ()
    {
        randomSource = Random.Range(0, 6);
        suctionSources[randomSource].pitch = Random.Range(0.6f, 1.5f);
	}
}
