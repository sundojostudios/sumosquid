﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuNavigator : MonoBehaviour {

	[SerializeField] private string moveHorizontal;
	[SerializeField] private string moveVertical;
	[SerializeField] private string selectHorizontal;
	[SerializeField] private string selectVertical;
	[SerializeField] private string p2moveHorizontal;
	[SerializeField] private string p2moveVertical;
	[SerializeField] private string p2selectHorizontal;
	[SerializeField] private string p2selectVertical;
	[SerializeField] private Color selectedColor = Color.blue;
	[SerializeField] private Button[] buttons;
	[SerializeField] private Slider[] sliders;
	[SerializeField] private Vector2[] indexes;
	private float wait = 0;
	private Vector2 selection = Vector2.zero;
	private Vector2 lastSelection = Vector2.zero;
	private float value = 0;

	void Update()
	{
		if (wait <= 0)
		{
			if (SumoInput.GetAxis(moveHorizontal, transform) != 0 || SumoInput.GetAxis(selectHorizontal, transform) != 0 || SumoInput.GetAxis(moveVertical, transform) != 0 || SumoInput.GetAxis(selectVertical, transform) != 0 || SumoInput.GetAxis(p2moveHorizontal, transform) != 0 || SumoInput.GetAxis(p2selectHorizontal, transform) != 0 || SumoInput.GetAxis(p2moveVertical, transform) != 0 || SumoInput.GetAxis(p2selectVertical, transform) != 0)
			{
				wait = 0.2f;
				lastSelection = selection;
				selection += new Vector2(Mathf.Round(SumoInput.GetAxis(moveHorizontal, transform) + SumoInput.GetAxis(selectHorizontal, transform) + SumoInput.GetAxis(p2moveHorizontal, transform) + SumoInput.GetAxis(p2selectHorizontal, transform)), Mathf.Round(-SumoInput.GetAxis(moveVertical, transform) + -SumoInput.GetAxis(selectVertical, transform) + -SumoInput.GetAxis(p2moveVertical, transform) + -SumoInput.GetAxis(p2selectVertical, transform)));
			}
		}
		else
		{
			wait -= Time.deltaTime;
		}

		if (SumoInput.GetAxis(moveHorizontal, transform) != 0 && SumoInput.GetAxis(selectHorizontal, transform) != 0 && SumoInput.GetAxis(moveVertical, transform) != 0 && SumoInput.GetAxis(selectVertical, transform) != 0 && SumoInput.GetAxis(p2moveHorizontal, transform) != 0 && SumoInput.GetAxis(p2selectHorizontal, transform) != 0 && SumoInput.GetAxis(p2moveVertical, transform) != 0 && SumoInput.GetAxis(p2selectVertical, transform) != 0)
		{
			wait = 0;
		}

		PointerEventData pointer = new PointerEventData(EventSystem.current);

		bool clicked = false;
		bool match = false;
		for (int i = 0; i < buttons.Length; i++)
		{
			if (Vector2.Distance(indexes[i], selection) < 0.5f && buttons[i].transform.gameObject.activeInHierarchy)
			{
				buttons[i].GetComponent<Image>().color = selectedColor;
				match = true;
				if(Input.anyKeyDown && clicked == false)
				{
					ExecuteEvents.Execute(buttons[i].gameObject, pointer, ExecuteEvents.submitHandler);
					selection = Vector2.zero;
					clicked = true;
				}
			}
			else
			{
				buttons[i].GetComponent<Image>().color = Color.white;
			}
		}

		for (int i = 0; i < sliders.Length; i++)
		{
			if (Vector2.Distance(indexes[i + buttons.Length], selection) < 0.5f && sliders[i].transform.gameObject.activeInHierarchy)
			{
				value += Mathf.Round(SumoInput.GetAxis(moveHorizontal, transform) + SumoInput.GetAxis(selectHorizontal, transform) + SumoInput.GetAxis(p2moveHorizontal, transform) + SumoInput.GetAxis(p2selectHorizontal, transform));
				sliders[i].image.color = selectedColor;
				match = true;
				sliders [i].value = value;
			}
			else
			{
				sliders[i].image.color = Color.white;
			}
		}
		if(!match)
		{
			selection = lastSelection;
		}
	}
}