﻿
using UnityEngine;
using System.Collections;
//Add this script to an Object to move it with microphone sound. Sound is translated to a float y and then Vector3.y


public class MicrophoneInk : MonoBehaviour
{
    
    private float MicLoudness;
    public float Loudness
        {
        get { return MicLoudness; }
        }

    private string _device;
    [SerializeField]
    private string micName;

    AudioClip _clipRecord = new AudioClip();
    int _sampleWindow = 128;

   [SerializeField] float inkSensetivity;

    //mic initialization
    void InitMic()
    {
        if (_device == null)
        { _device = Microphone.devices[0]; }
        _clipRecord = Microphone.Start(micName, true, 999, 44100);
    }

    void StopMicrophone()
    {
        Microphone.End(micName);
    }



    //get data from microphone into audioclip
    float LevelMax()
    {
        float levelMax = 0; 
        float[] waveData = new float[_sampleWindow];
        int test = Microphone.GetPosition(micName);
        int micPosition = Microphone.GetPosition(micName) - (_sampleWindow + 1); // null means the first microphone
        if (micPosition < 0) return 0;
        _clipRecord.GetData(waveData, micPosition);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _sampleWindow; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        return levelMax;
    }



    void Update()
    {
        // levelMax equals to the highest normalized value power 2, a small number because < 1
        MicLoudness = LevelMax();
        //  transform.localScale = new Vector3(Mathf.Sin(MicLoudness) * 10, Mathf.Sin(MicLoudness) *10);

        int j, i;
        Microphone.GetDeviceCaps(micName, out j, out i);
        //Debug.Log(Microphone.IsRecording(micName).ToString() + micName + MicLoudness.ToString());

        string[] low = Microphone.devices;
    }

    bool _isInitialized;
    // start mic when scene starts
    void OnEnable()
    {
        InitMic();
        _isInitialized = true;
    }

    //stop mic when loading a new level or quit application
    void OnDisable()
    {
        StopMicrophone();
    }

    void OnDestroy()
    {
        StopMicrophone();
    }


    // make sure the mic gets started & stopped when application gets focused
    void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            //Debug.Log("Focus");

            if (!_isInitialized)
            {
                //Debug.Log("Init Mic");
                InitMic();
                _isInitialized = true;
            }
        }
        if (!focus)
        {
            //Debug.Log("Pause");
            StopMicrophone();
            //Debug.Log("Stop Mic");
            _isInitialized = false;

        }
    }
    public bool Shoot()
    {
        if(MicLoudness > inkSensetivity)
        {
            return true;
        }
        return false;
    }
}
