﻿using UnityEngine;
using System.Collections;

public class FenceNotWarmUP : MonoBehaviour {

    [SerializeField] private Transform fence;
    [SerializeField] private ArenaDestroy arenaDestroy; 

	void Start () {
        if (!PreserveData.warmup)
        {
            fence.gameObject.SetActive(true);
            arenaDestroy.enabled = true;
        }
	}
}
