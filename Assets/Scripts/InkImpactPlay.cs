﻿using UnityEngine;
using System.Collections;

public class InkImpactPlay : MonoBehaviour {

	[SerializeField] Transform particleObject;

    public void Play()
    {
        particleObject.gameObject.SetActive(true);
    }
    public void Stop()
    {
        particleObject.gameObject.SetActive(false);
    }

}
