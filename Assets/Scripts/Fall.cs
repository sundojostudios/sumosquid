﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Fall : MonoBehaviour {

	[SerializeField] public Transform player;
    [SerializeField] public AudioSource fallSound;
    private bool isOut;
    [HideInInspector] AudioSource fallingSound;
    [HideInInspector] public bool definitelyNotPartOut;
    private int counter = 0;
    [SerializeField] private Slip slip;

    void Awake()
	{
        isOut = false;
        definitelyNotPartOut = true;
	}

    void OutCheck()
    {
        int matches = 0;
        int count = 1000;
        if (player != null)
        {
            if (slip != null)
            {
                slip.fastened = false;
            }

            matches = player.GetComponentsInChildren<Fall>().Length;
            count = matches;
            foreach (Fall cc2d in player.GetComponentsInChildren<Fall>())
            {
                if (cc2d.GetComponent<Fall>().definitelyNotPartOut)
                {
                    matches--;
                    if (cc2d.name == "Wobbly Squish & Rotate")
                    {
                        return;
                    }
                }
            }
        }
        else
        {
            isOut = true;
            return;
        }

        if (matches >= count - 4)
        {
            List<GameObject> gobjects = new List<GameObject>();

            for (int i = 0; i < player.childCount; i++)
            {
                gobjects.Add(player.GetChild(i).gameObject);
            }
            player.DetachChildren();
            player.transform.position = transform.position;
            foreach (GameObject gobject in gobjects)
            {
                gobject.transform.SetParent(player);
            }
            /*
                    fallSound.pitch = Random.Range (-1, 1);
                    fallSound.Play ();
                }*/
            isOut = true;
        }
    }

	void Update()
	{
        if (definitelyNotPartOut == false)
        {
            OutCheck();
        }

		if(isOut)
		{
            if (player != null)
            {
                player.localScale -= new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime);

                if(player.localScale.x <= 0)
                {
                    Destroy(player.gameObject);
                }
            }
            else
            {
                transform.localScale -= new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime);

                if(transform.localScale.x <= 0)
                {
                    Destroy(gameObject);
                }
            }
		}

        if (counter >= 10)
        {
            definitelyNotPartOut = false;
        }
        counter++;
	}

    void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Arena Part" && other.enabled)
		{
            definitelyNotPartOut = true;
            counter = 0;
		}
	}
}
