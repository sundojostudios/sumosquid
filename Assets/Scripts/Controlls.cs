﻿using UnityEngine;
using System.Collections;

public class Controlls : MonoBehaviour
{
	[SerializeField] private string moveHorizontal;
	[SerializeField] private string moveVertical;
	[SerializeField] private string selectHorizontal;
	[SerializeField] private string selectVertical;
	[SerializeField] private string launch;
	[SerializeField] private string launchB;
	[SerializeField] private string inkSprayButton;

	[SerializeField] public SelectLegAndLaunch selectLegAndLaunch;
	[SerializeField] public InkSpray inkSpray;
	[SerializeField] public Leg[] legs;
    [SerializeField] public Homing homing;
    
    void Awake ()
	{
		selectLegAndLaunch.selectHorizontal = selectHorizontal;
		selectLegAndLaunch.selectVertical = selectVertical;
		selectLegAndLaunch.moveHorizontal = moveHorizontal;
		selectLegAndLaunch.moveVertical = moveVertical;
		selectLegAndLaunch.launch = launch;
		selectLegAndLaunch.launchB = launchB;
		inkSpray.inkSprayButton = inkSprayButton;
		foreach (Leg leg in legs)
		{
			leg.moveHorizontal = moveHorizontal;
			leg.moveVertical = moveVertical;
			leg.selectHorizontal = selectHorizontal;
			leg.selectVertical = selectVertical;
		}
	}
}
