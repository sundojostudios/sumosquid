﻿using UnityEngine;
using System.Collections;

public class Slip : MonoBehaviour {

    [SerializeField] public Transform body;
    [SerializeField] public Transform legAnchor;
    [HideInInspector] public bool triggered = false;
    [HideInInspector] public bool fastened = false;

    //Depricated, but not the variables :P
}
