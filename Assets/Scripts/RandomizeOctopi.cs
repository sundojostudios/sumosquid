﻿using UnityEngine;
using System.Collections;

public class RandomizeOctopi : MonoBehaviour {

	[HideInInspector] public enum OctoColor
	{
		BLUE = 0,
		YELLOW = 1,
		PINK = 2,
		GREEN = 3
	}

	[HideInInspector] public struct OctopusStyle
	{
		public OctoColor color;
		public int eyes;
		public int eyelids;
		public int mouth;
		public int tentacles;
		public bool wounded;
	}

	[SerializeField] private SpriteRenderer player1Eye1;
	[SerializeField] private SpriteRenderer player1Eye2;
	[SerializeField] private SpriteRenderer player1Eyelid1;
	[SerializeField] private SpriteRenderer player1Eyelid2;
	[SerializeField] private SpriteRenderer player1Mouth;
	[SerializeField] private SkinnedMeshRenderer[] player1Tentacles;
	[SerializeField] private SpriteRenderer player1Head;
	[Space(20)]
	[SerializeField] private SpriteRenderer player2Eye1;
	[SerializeField] private SpriteRenderer player2Eye2;
	[SerializeField] private SpriteRenderer player2Eyelid1;
	[SerializeField] private SpriteRenderer player2Eyelid2;
	[SerializeField] private SpriteRenderer player2Mouth;
	[SerializeField] private SkinnedMeshRenderer[] player2Tentacles;
	[SerializeField] private SpriteRenderer player2Head;
	[Header("The order is: blue, yellow, pink, green.")]
	[Header("It goes through the colors for one style, and then the same for the next.")]
	[SerializeField] private Sprite[] eyes;
	[SerializeField] private Sprite[] eyelids;
	[SerializeField] private Sprite[] mouths;
	[SerializeField] private Sprite[] tentacles;
	[SerializeField] private Sprite[] heads;
	[Header("The order is: blue, yellow, pink, green.")]
	[Header("One style for all.")]
	[SerializeField] private Sprite[] woundedEyelids;
	[SerializeField] private Sprite[] woundedMouths;
	[SerializeField] private Sprite[] woundedHeads;

	public void RandomizePlayer1()
	{
		PreserveData.player1Style.eyes = Random.Range(0, eyes.Length / 4);
		PreserveData.player1Style.eyelids = Random.Range(0, eyelids.Length / 4);
		PreserveData.player1Style.mouth = Random.Range(0, mouths.Length / 4);
		PreserveData.player1Style.tentacles = Random.Range(0, tentacles.Length / 4);
		PreserveData.player1Style.wounded = false;

		setAppearance();
    }

    public void RandomizePlayer2()
    {
        PreserveData.player2Style.eyes = Random.Range(0, eyes.Length / 4);
        PreserveData.player2Style.eyelids = Random.Range(0, eyelids.Length / 4);
        PreserveData.player2Style.mouth = Random.Range(0, mouths.Length / 4);
        PreserveData.player2Style.tentacles = Random.Range(0, tentacles.Length / 4);
        PreserveData.player2Style.wounded = false;

        setAppearance();
    }

	public void setAppearance()
	{
		if (PreserveData.player1Style.wounded)
		{
			player1Eye2.sprite = player1Eye1.sprite = eyes[(int)PreserveData.player1Style.eyes * 4 + (int)PreserveData.player1Style.color];
			player1Eyelid2.sprite = player1Eyelid1.sprite = woundedEyelids[(int)PreserveData.player1Style.color];
			player1Mouth.sprite = woundedMouths[(int)PreserveData.player1Style.color];
			foreach (SkinnedMeshRenderer tentacle in player1Tentacles) {
				tentacle.material.mainTexture = tentacles[(int)PreserveData.player1Style.tentacles * 4 + (int)PreserveData.player1Style.color].texture;
			}
			player1Head.sprite = woundedHeads[(int)PreserveData.player1Style.color];
		}
		else
		{
			player1Eye2.sprite = player1Eye1.sprite = eyes[(int)PreserveData.player1Style.eyes * 4 + (int)PreserveData.player1Style.color];
			player1Eyelid2.sprite = player1Eyelid1.sprite = eyelids [(int)PreserveData.player1Style.eyelids * 4 + (int)PreserveData.player1Style.color];
			player1Mouth.sprite = mouths[(int)PreserveData.player1Style.mouth * 4 + (int)PreserveData.player1Style.color];
			foreach (SkinnedMeshRenderer tentacle in player1Tentacles) {
				tentacle.material.mainTexture = tentacles[(int)PreserveData.player1Style.tentacles * 4 + (int)PreserveData.player1Style.color].texture;
			}
			player1Head.sprite = heads[(int)PreserveData.player1Style.color];
		}

		if (PreserveData.player2Style.wounded)
		{
			player2Eye2.sprite = player2Eye1.sprite = eyes[(int)PreserveData.player2Style.eyes * 4 + (int)PreserveData.player2Style.color];
			player2Eyelid2.sprite = player2Eyelid1.sprite = woundedEyelids[(int)PreserveData.player2Style.color];
			player2Mouth.sprite = woundedMouths[(int)PreserveData.player2Style.color];
			foreach (SkinnedMeshRenderer tentacle in player2Tentacles) {
				tentacle.material.mainTexture = tentacles[(int)PreserveData.player2Style.tentacles * 4 + (int)PreserveData.player2Style.color].texture;
			}
			player2Head.sprite = woundedHeads[(int)PreserveData.player2Style.color];
		}
		else
		{
			player2Eye2.sprite = player2Eye1.sprite = eyes[(int)PreserveData.player2Style.eyes * 4 + (int)PreserveData.player2Style.color];
			player2Eyelid2.sprite = player2Eyelid1.sprite = eyelids[(int)PreserveData.player2Style.eyelids * 4 + (int)PreserveData.player2Style.color];
			player2Mouth.sprite = mouths[(int)PreserveData.player2Style.mouth * 4 + (int)PreserveData.player2Style.color];
			foreach (SkinnedMeshRenderer tentacle in player2Tentacles) {
				tentacle.material.mainTexture = tentacles[(int)PreserveData.player2Style.tentacles * 4 + (int)PreserveData.player2Style.color].texture;
			}
			player2Head.sprite = heads[(int)PreserveData.player2Style.color];
		}
	}

	void Start()
	{
		setAppearance();
	}
}
