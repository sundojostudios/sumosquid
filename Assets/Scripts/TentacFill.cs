﻿using UnityEngine;
using System.Collections;

public class TentacFill : MonoBehaviour {
    
    [SerializeField]
    Transform myLeg;
    [SerializeField]
    Transform targetLeg;
    [SerializeField]
    Vector3[] vertices;
    Vector3[] verticeReference;
    void Awake ()
    {
        vertices = GetComponent<MeshFilter>().mesh.vertices;
        verticeReference = GetComponent<MeshFilter>().mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i].z = -0.1f;
        }
        for (int i = 0; i < vertices.Length; i++)
        {
            verticeReference[i].z = -0.1f;
        }
    }
	
	
	void Update ()
    {
        vertices[1] =  new Vector3(Mathf.Sin(-Mathf.Deg2Rad * (targetLeg.eulerAngles.z - myLeg.eulerAngles.z)) * verticeReference[1].x, Mathf.Cos(-Mathf.Deg2Rad * (targetLeg.eulerAngles.z - myLeg.eulerAngles.z)) * verticeReference[1].y, 0);
       // vertices[3] = new Vector2(transform.position.x - anchor.transform.position.x - 0.4f, transform.position.y - anchor.transform.position.y);

        GetComponent<MeshFilter>().mesh.vertices = vertices;
    }

    

}
