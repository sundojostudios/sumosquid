﻿using UnityEngine;
using System.Collections;

public class CrabFall : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Arena Out"))
        {
            transform.parent.GetComponent<CrabScript>().StartFall();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Arena Out"))
        {
            transform.parent.GetComponent<CrabScript>().StartFall();
        }
    }
}
