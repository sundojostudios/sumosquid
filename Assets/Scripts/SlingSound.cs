﻿using UnityEngine;
using System.Collections;

public class SlingSound : MonoBehaviour
{

    public AudioClip slingClip;
    public AudioClip launchClip;

    public AudioSource source;

    private bool slingSoundBool;
    public bool sling
    {
        get { return slingSoundBool; }
        set { slingSoundBool = value; }
    }
    private bool hasPlayed;
    public bool played
    {
        get { return hasPlayed; }
        set { hasPlayed = value; }
    }
    private bool launchSoundBool;
    public bool launch
    {
        get { return launchSoundBool; }
        set { launchSoundBool = value; }
    }


    void Update()
    {
        if (slingSoundBool && !source.isPlaying && !hasPlayed)
        {
            PlaySlingSound();
            hasPlayed = true;
        }

        if (launchSoundBool && !source.isPlaying)
        {
            PlayLaunchSound();
        }
    }

    void PlaySlingSound()
    {
        source.PlayOneShot(slingClip, 1f);
        
    }

    void PlayLaunchSound()
    {
        source.PlayOneShot(launchClip, 1f);
        launchSoundBool = false;
    }
} 
