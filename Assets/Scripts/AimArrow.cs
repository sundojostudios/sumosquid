﻿using UnityEngine;
using System.Collections;

public class AimArrow : MonoBehaviour
{

    bool isScaling = false;

    void Update()
    {
        if(isScaling)
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y + 0.01f, transform.localScale.z);
        }
        if (transform.localScale.y >= 1.6f)
        {
            isScaling = false;
        }
    }
    void OnEnable()
    {
        transform.localScale = new Vector3(transform.localScale.x, 0.5f, transform.localScale.z);
        isScaling = true;
    }
    
}
