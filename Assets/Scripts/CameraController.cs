﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class CameraController : MonoBehaviour
{

    [SerializeField] private Transform player1Body;
    [SerializeField] private Transform player2Body;
	[SerializeField] private Transform center;
	private float shakePartTime;
	private Vector3 shakePosition;
	private Vector3 newShakePosition;

	private Vector3 position;

    [HideInInspector] public Vector2 shakeAmount = Vector2.zero;

    void Start()
    {
        position = transform.position;
    }

    void Update ()
    {
		if (player1Body != null && player2Body != null)
		{
			Vector2 tmpPosition = (player1Body.position + player2Body.position) / 2;
			position = Vector3.Lerp (position, new Vector3 (tmpPosition.x, tmpPosition.y, -10), Time.deltaTime * 1);
			GetComponent<Camera> ().orthographicSize = Mathf.Lerp (GetComponent<Camera> ().orthographicSize, 14 + Vector2.Distance (player1Body.position, player2Body.position) * 0.2f, Time.deltaTime * 1);
			if (GetComponent<Camera> ().orthographicSize < 14)
			{
				GetComponent<Camera> ().orthographicSize = 14;
			}
		}
		else if (player1Body != null)
		{
            Vector3 Temp = new Vector3(player1Body.position.x, player1Body.position.y, player1Body.position.z - 10);
			position = Vector3.Lerp (position, Temp, Time.deltaTime);
		}
		else if (player2Body != null)
		{
            Vector3 Temp = new Vector3(player2Body.position.x, player2Body.position.y, player2Body.position.z - 10);
            position = Vector3.Lerp (position, Temp, Time.deltaTime);
		}
		else
		{
			position = Vector3.Lerp (position, new Vector3(center.position.x, center.position.y, -10), Time.deltaTime);
			GetComponent<Camera> ().orthographicSize = Mathf.Lerp (GetComponent<Camera> ().orthographicSize, 18, Time.deltaTime * 1);
		}

		if (shakePartTime <= 0.02f)
		{
			newShakePosition = new Vector3 (Random.Range (-shakeAmount.x, shakeAmount.x), Random.Range (-shakeAmount.y, shakeAmount.y), 0) / 10;
			shakePartTime += 0.04f;
		}
		shakePartTime -= Time.deltaTime;
		shakePosition = Vector2.Lerp(shakePosition, newShakePosition, Time.deltaTime * 10);

		transform.position = position + new Vector3(Mathf.Clamp(shakePosition.x, -1, 1), Mathf.Clamp(shakePosition.y, -1, 1), Mathf.Clamp(shakePosition.z, -1, 1));

        GamePad.SetVibration((PlayerIndex)0, shakePosition.x, shakePosition.y);
        GamePad.SetVibration((PlayerIndex)1, shakePosition.x, shakePosition.y);

        if(shakeAmount.magnitude >= 0.1f)
        {
            GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = true;
            GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain>().enabled = true;
            GetComponent<UnityStandardAssets.ImageEffects.Fisheye>().enabled = true;
        }
        else
        {
            GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = false;
            GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain>().enabled = false;
            GetComponent<UnityStandardAssets.ImageEffects.Fisheye>().enabled = false;
        }
    }

    void FixedUpdate()
    {
        shakeAmount /= 2;
    }
}
