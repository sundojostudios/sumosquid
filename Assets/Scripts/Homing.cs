﻿using UnityEngine;
using System.Collections;

public class Homing : MonoBehaviour {

	[SerializeField] private Transform otherPlayerBody;
	private float lastDistance = 100000;
	[SerializeField] [Range(0, 2)] private float directionLimit = 0.1f;

	void Update ()
    {
        if (otherPlayerBody == null)
        {
            return;
        }

		float distance = Vector2.Distance(transform.position, otherPlayerBody.position);
        Vector2 direction = (transform.position - otherPlayerBody.position).normalized;
		if (distance >= lastDistance || 
			GetComponent<Rigidbody2D>().velocity.magnitude < otherPlayerBody.GetComponent<Rigidbody2D>().velocity.magnitude || 
			Vector2.Distance(GetComponent<Rigidbody2D>().velocity.normalized, direction) <= directionLimit
		)
		{
			lastDistance = distance;
			return;
		}
		lastDistance = distance;

        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        GetComponent<Rigidbody2D>().velocity -= (velocity.normalized * velocity.magnitude) / distance;
        GetComponent<Rigidbody2D>().velocity += (-direction * velocity.magnitude) / distance;
    }
}
