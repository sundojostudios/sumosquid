﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorPlayer : MonoBehaviour {

	[SerializeField] public Slime slime;
	[SerializeField] private Color colorA;
	[SerializeField] private Color colorB;
	private Color colorALast;
	private Color colorBLast;
	[SerializeField] private Slider sliderA;
	[SerializeField] private Slider sliderB;

	void Start()
	{
		if (name == "Player 2")
		{
			sliderA.value = PreserveData.hue1A;
			sliderB.value = PreserveData.hue1B;
		}
		else
		{
			sliderA.value = PreserveData.hue2A;
			sliderB.value = PreserveData.hue2B;
		}
		SetColorA();
		SetColorB();
	}

	void saveData()
	{

		if (name == "Player 2")
		{
			PreserveData.hue1A = sliderA.value;
			PreserveData.hue1B = sliderB.value;
		}
		else
		{
			PreserveData.hue2A = sliderA.value;
			PreserveData.hue2B = sliderB.value;
		}
	}

	void Update()
	{
		if(colorALast != colorA || colorBLast != colorB)
		{
			ChangeColor();
			colorALast = colorA;
			colorBLast = colorB;
		}
	}

	void SetColorA()
	{
		colorA = RGBHSV.HSVToRGB(sliderA.value / 255, 1, 1);
		saveData();
	}

	void SetColorB()
	{
		colorB = RGBHSV.HSVToRGB(sliderB.value / 255, 1, 1);
		saveData();
	}

	void ChangeColor () {
//		slime.slimeColor = colorA;

		foreach(SpriteRenderer child in GetComponentsInChildren<SpriteRenderer>())
		{
			if (child.name == "Ball")
			{
				child.color = colorA;
			}
            if (child.name == "Eyebrow1")
            {
                child.color = colorA;
            }
            if (child.name == "Eyebrow2")
            {
                child.color = colorA;
            }
            if (child.name == "Mouth")
            {
                child.color = colorA;
            }

            if (child.name.Substring(0, 3) == "Leg")
			{
				if (child.name.Substring(6) == "Start")
				{
					child.color = colorA;
				}
				else if (child.name.Substring(6) == "Middle")
				{
					child.color = (colorA + colorB) / 2;
				}
				else if (child.name.Substring(6) == "Middle 1")
				{
					child.color = (colorA + colorB) / 2;
				}
				else if (child.name.Substring(6) == "Middle 2")
				{
					child.color = (colorA + colorB) / 2;
				}
				else if (child.name.Substring(6) == "End")
				{
					child.color = colorB;
				}
			}
		}

		foreach (LineRenderer child in GetComponentsInChildren<LineRenderer>())
		{
			if (child.name.Substring(0, 3) == "Leg")
			{
				if (child.name.Substring(6) == "Middle")
				{
					child.SetColors((colorA + colorB) / 4, colorA);
				}
				else if (child.name.Substring(6) == "Middle 1")
				{
					child.SetColors(((colorA + colorB) / 4) * 2, colorA);
				}
				else if (child.name.Substring(6) == "Middle 2")
				{
					child.SetColors(((colorA + colorB) / 4) * 3, colorA);
				}
				else if (child.name.Substring(6) == "End")
				{
					child.SetColors(colorB, (colorA + colorB) / 2);
				}
			}
		}
        foreach(Renderer child in GetComponentsInChildren<Renderer>())
        {
            if (child.name == "Plane")
            {
				child.materials[0].SetColor("_Color", colorA);
            }
        }
	}
}
