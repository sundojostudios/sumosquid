﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour
{
    public bool fade = false;
    SpriteRenderer[] myRenderers;
    PulsatingOpacity[] myTurnoffs;
    PathTransforming[] myTurnoffs2;
    void Start()
    {
        myRenderers = GetComponentsInChildren<SpriteRenderer>();
        myTurnoffs = GetComponentsInChildren<PulsatingOpacity>();
        myTurnoffs2 = GetComponentsInChildren<PathTransforming>();
        foreach (SpriteRenderer renderer in myRenderers)
        {
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 1);
        }
        
    }
    void Update ()
    {
	    if (fade)
        {
            foreach (SpriteRenderer renderer in myRenderers)
            {
                renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, renderer.color.a - 0.025f);
            }
            foreach (PulsatingOpacity turnoff in myTurnoffs)
            {
                turnoff.enabled = false;
            }
            foreach (PathTransforming turnoff in myTurnoffs2)
            {
                turnoff.enabled = false;
            }
        }
	}
}
