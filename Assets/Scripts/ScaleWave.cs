﻿using UnityEngine;
using System.Collections;

public class ScaleWave : MonoBehaviour {
    
    [SerializeField]
    [Range(0.05f, 5f)]
    private float Speed = 0.005f;
    public float ScalingSpeed
    {
       get{return Speed;}
       set{Speed = value;}
    }
    
    [SerializeField]
    [Range(0.005f, 5.0f)]
    private float size = 0.05f;
    public float scaleSize
    {
        get { return size; }
        set { size = value; }
    }
  
  
    [SerializeField]
    [Range(-5.0f, 5.0f)]
    private float RotationSpeed = 0.0f;
    public float SpeedOfRotation
    {
        get { return RotationSpeed; }
        set { RotationSpeed = value; }
    }

    private Vector3 scale;

    void Start()
    {
        scale = transform.localScale;
    }

    void Update ()
    {
        transform.localScale = scale + Vector3.one * Mathf.Sin(Time.time * Speed) * size;
        transform.Rotate(0, 0, RotationSpeed);
    }
}
