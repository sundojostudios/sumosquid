﻿using UnityEngine;
using System.Collections;

public class ParticlesOneShot : MonoBehaviour {
 
	void Update () {
		if(GetComponent<ParticleSystem>().IsAlive() == false)
		{
			Destroy(gameObject);
		}
	}
}
