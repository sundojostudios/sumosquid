﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scene3Event : MonoBehaviour {

    [SerializeField] private SpriteRenderer[] turtleSprites;
    private List<int> sortingOrgs;
    private List<float> scaleYOrgs;

	void Start()
    {
        sortingOrgs = new List<int>();
        scaleYOrgs = new List<float>();
        foreach (SpriteRenderer sr in turtleSprites)
        {
            sortingOrgs.Add(sr.sortingOrder);
            scaleYOrgs.Add(sr.transform.localScale.y);
        }
	}
	
	void Update()
    {
        for(int i = 0; i < turtleSprites.Length; i++)
        {
            turtleSprites[i].sortingOrder = sortingOrgs[i] + (int)(Mathf.Sin(Time.time * 0.4f) * 10);
            turtleSprites[i].transform.localScale = new Vector3(turtleSprites[i].transform.localScale.z, scaleYOrgs[i] + (Mathf.Cos(Time.time * 0.4f) * 0.1f) - 0.1f, turtleSprites[i].transform.localScale.z);
            turtleSprites[i].color = new Color(1, 1, 1, Mathf.Sin(Time.time * 0.4f) * 0.25f + 0.75f);
        }
	}
}
