﻿using UnityEngine;
using System.Collections;

public class Line : MonoBehaviour
{

	[SerializeField] public Transform other;

	void Update ()
	{
		GetComponent<LineRenderer> ().SetPosition (0, transform.position);
		GetComponent<LineRenderer> ().SetPosition (1, other.position);
	}
}
