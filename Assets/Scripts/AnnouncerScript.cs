﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnnouncerScript : MonoBehaviour
{

   
    [SerializeField]
    private AudioSource[] player1Win;
    [SerializeField]
    private AudioSource[] player2Win;
    [SerializeField]
    private AudioSource[] endOfRoundSound;
    [SerializeField]
    private AudioSource[] tieSound;
    [SerializeField]
    private AudioSource countDown;
    [SerializeField]
    private Transform winnerAnimation;
    
    //[HideInInspector]
    public bool playTie;
    public bool tieBool
    {
        set { playTie = value; }
    }
    //[HideInInspector]
    public bool player1WinBool;
    public bool p1winBool
    {
        set { player1WinBool = value; }
    }
    //[HideInInspector]
    public bool player2WinBool;
    public bool p2winBool
    {
        set { player2WinBool = value; }
    }


    void Update()
    {
        RoundAnnouncement();
    }

    void PlayTieAnnouncement()
    {
        
      
    }

    void PlayRoundAnnouncement()
    {
        if (PreserveData.round == 0)
        {
            
        }
        else if (PreserveData.round == 1)
        {
            
        }
    }

    void RoundAnnouncement()
    {
        /*
        if (player1WinBool)
        {
            player1Win[Random.Range(0, 2)1].Play();
            player1WinBool = false;
            //winnerAnimation.gameObject.SetActive(true);
            //winnerAnimation.GetComponent<RoundWinner>().Player1();                       
        }
      */
        if(player1WinBool && PreserveData.winsPlayer1 == 1)
        {
            player1WinBool = false;
            player1Win[2].Play();
        }
        else if (player2WinBool && PreserveData.winsPlayer2 == 1)
        {
            player2WinBool = false;
            player2Win[0].Play();
        }
        else if (player2WinBool || player1WinBool)
        {
            endOfRoundSound[Random.Range(0, 3)].Play();
            player2WinBool = false;
            player1WinBool = false;
            //winnerAnimation.gameObject.SetActive(true);
            //winnerAnimation.GetComponent<RoundWinner>().Player2();
        }

        else if (playTie)
        {
            tieSound[Random.Range(0, 2)].Play();
            playTie = false;
            //winnerAnimation.gameObject.SetActive(true);
            //winnerAnimation.GetComponent<RoundWinner>().Tie();
        }
        
    }
}