﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Drag : MonoBehaviour {

    [SerializeField] public float linear;
    [SerializeField] public float angular;

    void Update () {
        //GetComponent<Rigidbody2D>().velocity /= linear * Time.deltaTime;
        //GetComponent<Rigidbody2D>().angularVelocity /= angular * Time.deltaTime;
        GetComponent<Rigidbody2D>().drag = 100000;
    }
}
