﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour
{
    SpriteRenderer[] myRenderers;
   

    void Start()
    {
        myRenderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer renderer in myRenderers)
        {
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0);
        }
        
    }
	void Update ()
    {
        foreach(SpriteRenderer renderer in myRenderers)
        {
            renderer.color += new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0.025f);
        }
        
    }
}
