﻿using UnityEngine;
using System.Collections;

public class MenuTransition : MonoBehaviour
{
    bool transition = false;
    bool once = false;
    float starttime;
    [SerializeField] float targettime = 2;
    [SerializeField] private Transform[] fadeBelow;
    [SerializeField]
    private Transform objectToActivate;
    void Update ()
    {
        if(Input.anyKeyDown)
        {
            transition = true;
        }
        if (transition)
        {
            foreach(Transform fade in fadeBelow)
            {
                fade.GetComponent<FadeOut>().fade = true;
            }
        }
        if (GetComponent<SpriteRenderer>().color.a <= 0)
        {
            if (!once)
            {
                StartCoroutine(destroy(1));
                once = true;
            }
        }
        
    }

    IEnumerator destroy(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this);
    }

    void OnDestroy()
    {
        objectToActivate.gameObject.SetActive(true);
    }
}
