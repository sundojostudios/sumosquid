﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SumoInput
{
	[System.Serializable] public struct KeyBinding
	{
		public string name;
		public KeyCode negativeButton;
		public KeyCode positiveButton;
		public KeyCode altNegativeButton;
		public KeyCode altPositiveButton;
	}

	[System.Serializable] public struct KeyMap
	{
		public string mappingName;
		public KeyBinding[] bindings;
	}

	public static KeyMap keyMapping;

	private static bool _GetButton(string buttonName)
	{
		try
		{
            if(Input.GetButton(buttonName))
			{
				return true;
			}
		}
		catch {}

		KeyBinding match = new KeyBinding();
		match.name = "148515414651451451438465";
		foreach (KeyBinding binding in keyMapping.bindings)
		{
			if (binding.name == buttonName)
			{
				match = binding;
			}
		}
		if (match.name == "148515414651451451438465")
		{
			Debug.LogError("Button: '" + buttonName + "' doesn't exist");
		}

		return Input.GetKey(match.negativeButton) ||
			Input.GetKey(match.positiveButton) ||
			Input.GetKey(match.altNegativeButton) ||
			Input.GetKey(match.altPositiveButton)
			;
	}

	private static bool _GetButtonDown(string buttonName)
    {
		try
		{
            if(Input.GetButtonDown(buttonName))
			{
				return true;
			}
		}
		catch {}

		KeyBinding match = new KeyBinding();
		match.name = "148515414651451451438465";
		foreach (KeyBinding binding in keyMapping.bindings)
		{
			if (binding.name == buttonName)
			{
				match = binding;
			}
		}
		if (match.name == "148515414651451451438465")
		{
			Debug.LogError("Button: '" + buttonName + "' doesn't exist");
		}

		return Input.GetKeyDown(match.negativeButton) ||
			Input.GetKeyDown(match.positiveButton) ||
			Input.GetKeyDown(match.altNegativeButton) ||
			Input.GetKeyDown(match.altPositiveButton)
			;
	}

	private static bool _GetButtonUp(string buttonName)
	{
		try
		{
            if(Input.GetButtonUp(buttonName))
			{
				return true;
			}
		}
		catch {}

		KeyBinding match = new KeyBinding();
		match.name = "148515414651451451438465";
		foreach (KeyBinding binding in keyMapping.bindings)
		{
			if (binding.name == buttonName)
			{
				match = binding;
			}
		}
		if (match.name == "148515414651451451438465")
		{
			Debug.LogError("Button: '" + buttonName + "' doesn't exist");
		}

		return Input.GetKeyUp(match.negativeButton) ||
			Input.GetKeyUp(match.positiveButton) ||
			Input.GetKeyUp(match.altNegativeButton) ||
			Input.GetKeyUp(match.altPositiveButton)
			;
	}

	private static float _GetAxis(string axisName)
	{
		KeyBinding match = new KeyBinding();
		match.name = "148515414651451451438465";
		foreach (KeyBinding binding in keyMapping.bindings)
		{
			if (binding.name == axisName)
			{
				match = binding;
			}
		}
		if (match.name == "148515414651451451438465")
		{
			//Debug.LogError("Axis: '" + axisName + "' doesn't exist");
		}

		float input = 0;
		if (Input.GetKey(match.negativeButton) || Input.GetKey(match.altNegativeButton))
		{
			input--;
		}
		if (Input.GetKey(match.positiveButton) || Input.GetKey(match.altPositiveButton))
		{
			input++;
		}

		try
		{
			input = Mathf.Clamp(input + Input.GetAxis(axisName), -1, 1);
		}
		catch {}

		return input;
	}


	public static float GetAxis(string axisName, Transform transform)
	{
		if((PreserveData.player1AI && transform.tag == "Player1") || (PreserveData.player2AI && transform.tag == "Player2"))
		{
			return transform.GetComponent<AIController>().GetAxis(axisName);
		}
		else
		{
			return _GetAxis(axisName);
		}
	}

	public static bool GetButton(string buttonName, Transform transform)
	{
		if ((PreserveData.player1AI && transform.tag == "Player1") || (PreserveData.player2AI && transform.tag == "Player2"))
		{
			return transform.GetComponent<AIController>().GetButton(buttonName);
		}
		else
		{
			return _GetButton(buttonName);
		}
	}

	public static bool GetButtonDown(string buttonName, Transform transform)
	{
		if ((PreserveData.player1AI && transform.tag == "Player1") || (PreserveData.player2AI && transform.tag == "Player2"))
		{
			return transform.GetComponent<AIController>().GetButtonDown(buttonName);
		}
		else
		{
			return _GetButtonDown(buttonName);
		}
	}

	public static bool GetButtonUp(string buttonName, Transform transform)
	{
		if ((PreserveData.player1AI && transform.tag == "Player1") || (PreserveData.player2AI && transform.tag == "Player2"))
		{
			return transform.GetComponent<AIController>().GetButtonUp(buttonName);
		}
		else
		{
			return _GetButtonUp(buttonName);
		}
	}

}

