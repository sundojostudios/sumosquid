﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectLegAndLaunch : MonoBehaviour
{
    [SerializeField] public Transform thingy;
    [SerializeField] public Transform[] starts;
    [SerializeField] public Transform[] legs;
    [SerializeField] public Transform SuctionCup;
    [SerializeField] private Color notSelectedLegsColor;
    [HideInInspector] public float jumpCoolDown;
    [SerializeField] public float jumpCoolDownMax;
    private Vector2 anchtmp;
    [SerializeField] private GameObject jumpFX;
    [SerializeField] private ParticleSystem sweatFX;

    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private AudioSource jumpSource;

    [SerializeField] private float minStickMovement;

    [SerializeField] private Transform aimArrow;
    [SerializeField] private AudioSource stretchSound;
    [HideInInspector] public string selectHorizontal;
	[HideInInspector] public string selectVertical;
	[HideInInspector] public string moveHorizontal;
	[HideInInspector] public string moveVertical;
	[HideInInspector] public string launch;
	[HideInInspector] public string launchB;
    [HideInInspector] public bool isInSLingshot = false;

    [SerializeField] private float timeUntilSling = 0.8f;
	private float slingCounter;
	private bool fastendedTrigger;
    public bool soundSlingshot;
    public float slingSoundCounter;

	[SerializeField] private ParticleSystem inbromsningFX;
	private Vector2 lastVelocity;

	[SerializeField] public float timeSinceSlingshot = 1000000000;
	[SerializeField] public float timeSinceSlingshotInputBlockLimit = 0.5f;

	private float buildUpTime = 0.25f;

	float lastUsedSelectHorizontal = 0;
	float lastUsedSelectVertical = 0;
	float lastUsedMoveHorizontal = 0;
	float lastUsedMoveVertical = 0;

	[SerializeField] private float DelayedSlingTime = 0.15f;
	private float timeSinceSlingshotCancel = 1000000000;

    void Start()
	{
		jumpCoolDown = 0;
		slingCounter = 0;
		
		foreach (Transform leg in legs)
		{
			leg.GetComponent<Slip>().legAnchor.GetComponent<Leg>().active = true;
			leg.GetComponent<Slip>().legAnchor.GetComponent<SpriteRenderer>().color = Color.magenta;
		}
	}

	Quaternion LookAt2D(Vector3 position, Vector3 targetPosition, int axis = 2)
	{
		Vector3 diff = targetPosition - position;
		diff.Normalize();

		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		if(axis == 0)
		{
			return Quaternion.Euler(rot_z - 90, 0f, 0f);
		}
		else if(axis == 1)
		{
			return Quaternion.Euler(0f, rot_z - 90, 0f);
		}
		else
		{
			return Quaternion.Euler(0f, 0f, rot_z - 90);
		}
	}

	void Sling()
	{     
		GetComponent<Rigidbody2D> ().isKinematic = true;
		GetComponent<Rigidbody2D> ().isKinematic = false;

        transform.GetComponent<Rigidbody2D>().AddForce(SumoInput.GetAxis(selectHorizontal, transform) * Vector2.right * -1550 + SumoInput.GetAxis(selectVertical, transform) * Vector2.up * -1550, ForceMode2D.Impulse);
        transform.GetComponent<Rigidbody2D>().AddForce(SumoInput.GetAxis(moveHorizontal, transform) * Vector2.right * -1550 + SumoInput.GetAxis(moveVertical, transform) * Vector2.up * -1550, ForceMode2D.Impulse);


		Transform fx = (Instantiate(jumpFX, new Vector3(transform.position.x, transform.position.y, jumpFX.transform.position.z), Quaternion.identity) as GameObject).transform;
		fx.rotation = LookAt2D(fx.position, (Vector2)fx.position + GetComponent<Rigidbody2D>().velocity);
        SetTo setTo = fx.gameObject.AddComponent<SetTo>();
        setTo.target = transform;
        setTo.speed = 4;
        jumpSource.pitch = Random.Range(0.8f, 1.2f);
        jumpSource.Play();
        
		timeSinceSlingshot = 0;
		timeSinceSlingshotCancel = 1000000000000;
	}

	void OnFastenedUp()
	{
		//GetComponent<SpringJoint2D>().enabled = true;
		//GetComponent<SetTo>().enabled = false;

		transform.parent = thingy.parent;
		foreach(Transform start in starts)
		{
			start.GetComponent<DistanceJoint2D>().connectedBody = thingy.GetComponent<Rigidbody2D>();
			start.GetComponent<HingeJoint2D>().connectedBody = thingy.GetComponent<Rigidbody2D>();
		}
	}

	void OnFastenedDown()
	{
		//GetComponent<SpringJoint2D>().enabled = false;
        GetComponent<SetTo>().enabled = true;

		transform.parent = thingy;
		foreach (Transform start in starts)
		{
			start.GetComponent<DistanceJoint2D>().connectedBody = GetComponent<Rigidbody2D>();
			start.GetComponent<HingeJoint2D>().connectedBody = GetComponent<Rigidbody2D>();
		}

		timeSinceSlingshot = 1000000000;
	}

	void Update()
	{
		if (Mathf.Abs(SumoInput.GetAxis(selectHorizontal, transform)) > 0.5f || Mathf.Abs(SumoInput.GetAxis(selectVertical, transform)) > 0.5f)
		{
			lastUsedSelectHorizontal = SumoInput.GetAxis(selectHorizontal, transform);
			lastUsedSelectVertical = SumoInput.GetAxis(selectVertical, transform);
		}

		if (Mathf.Abs(SumoInput.GetAxis(moveHorizontal, transform)) > 0.5f || Mathf.Abs(SumoInput.GetAxis(moveVertical, transform)) > 0.5f)
		{
			lastUsedMoveHorizontal = SumoInput.GetAxis(moveHorizontal, transform);
			lastUsedMoveVertical = SumoInput.GetAxis(moveVertical, transform);
		}

		/*inbromsningFX.transform.rotation = LookAt2D(Vector3.zero, -GetComponent<Rigidbody2D>().velocity);
		inbromsningFX.transform.eulerAngles = Vector3.Lerp(new Vector3(inbromsningFX.transform.eulerAngles.x, 90, -90), new Vector3(-inbromsningFX.transform.eulerAngles.z - 90, 90, -90), Time.deltaTime * 20);
		if (GetComponent<Rigidbody2D>().velocity.magnitude <= lastVelocity.magnitude)
		{
			if (!inbromsningFX.isPlaying)
			{
				inbromsningFX.Play();
			}
		}
		else
		{
			//inbromsningFX.Stop();
		}
		lastVelocity = GetComponent<Rigidbody2D>().velocity;*/

		timeSinceSlingshot += Time.deltaTime;

		bool match = false;
		int matches = 0;
		foreach (Transform leg in legs)
		{
			if (leg.GetComponent<Slip> ().fastened) {
				match = true;
				matches++;
				leg.GetComponent<Slip> ().legAnchor.GetComponent<Leg> ().active = false;
				leg.GetComponent<Slip> ().legAnchor.GetChild(0).gameObject.SetActive (true);
                




            }
			else
			{
				leg.GetComponent<Slip> ().legAnchor.GetComponent<Leg> ().active = true;
				leg.GetComponent<Slip> ().legAnchor.GetChild(0).gameObject.SetActive (false);
                



            }
		}

        if (matches < 6)
        {
            stretchSound.Stop();
            isInSLingshot = false;
            aimArrow.gameObject.SetActive(false);
            slingCounter = 0;
            slingSoundCounter = 0;
        }

		if (match)
		{
			if (slingCounter >= timeUntilSling)
			{
				timeSinceSlingshotCancel = 0;

                aimArrow.gameObject.SetActive(true);
				if (!stretchSound.isPlaying) 
				{
					stretchSound.Play();
				}
                isInSLingshot = true;
				if (slingCounter >= timeUntilSling + buildUpTime)
				{
                    transform.GetComponent<Rigidbody2D>().AddForce(-SumoInput.GetAxis(selectHorizontal, transform) * Vector2.right * -32000 * Time.deltaTime + -SumoInput.GetAxis(selectVertical, transform) * Vector2.up * -32000 * Time.deltaTime, ForceMode2D.Impulse);
                    transform.GetComponent<Rigidbody2D>().AddForce(-SumoInput.GetAxis(moveHorizontal, transform) * Vector2.right * -32000 * Time.deltaTime + -SumoInput.GetAxis(moveVertical, transform) * Vector2.up * -32000 * Time.deltaTime, ForceMode2D.Impulse);
                    sweatFX.Play();
                }
				else
                {
                    transform.GetComponent<Rigidbody2D>().AddForce(-SumoInput.GetAxis(selectHorizontal, transform) * Vector2.right * -3000 * Time.deltaTime + -SumoInput.GetAxis(selectVertical, transform) * Vector2.up * -3000 * Time.deltaTime, ForceMode2D.Impulse);
                    transform.GetComponent<Rigidbody2D>().AddForce(-SumoInput.GetAxis(moveHorizontal, transform) * Vector2.right * -3000 * Time.deltaTime + -SumoInput.GetAxis(moveVertical, transform) * Vector2.up * -3000 * Time.deltaTime, ForceMode2D.Impulse);
                }
            }
			else
			{
                sweatFX.Stop();
				if (SumoInput.GetButton(launch, transform))
				{
					GetComponent<Rigidbody2D>().AddForce(-SumoInput.GetAxis(selectHorizontal, transform) * Vector2.right * 30000 * Time.deltaTime + -SumoInput.GetAxis(selectVertical, transform) * Vector2.up * 30000 * Time.deltaTime, ForceMode2D.Force);
                   
                }
				if (SumoInput.GetButton(launchB, transform))
				{
					GetComponent<Rigidbody2D>().AddForce(-SumoInput.GetAxis(moveHorizontal, transform) * Vector2.right * 30000 * Time.deltaTime + -SumoInput.GetAxis(moveVertical, transform) * Vector2.up * 30000 * Time.deltaTime, ForceMode2D.Force);
                    
                }
			}
			
			if((Mathf.Abs(SumoInput.GetAxis(selectHorizontal, transform)) > 0.5f || Mathf.Abs(SumoInput.GetAxis(selectVertical, transform)) > 0.5f) &&
				(Mathf.Abs(SumoInput.GetAxis(moveHorizontal, transform)) > 0.5f || Mathf.Abs(SumoInput.GetAxis(moveVertical, transform)) > 0.5f) &&
				SumoInput.GetButton(launch, transform) && SumoInput.GetButton(launchB, transform) &&
				matches >= 6)
			{
                
				slingCounter += Time.deltaTime;
                slingSoundCounter += Time.deltaTime;
                if (slingSoundCounter >= 0.4f)
                {
                    //GetComponent<SlingSound>().sling = true;       You are giving me errors!
                }
                
              
            }
			else
			{
				if (slingCounter >= timeUntilSling)
				{
					Sling();
                }

				stretchSound.Stop();
                isInSLingshot = false;
                aimArrow.gameObject.SetActive(false);
				slingCounter = 0;
                slingSoundCounter = 0;
                //GetComponent<SlingSound>().sling = false;          You are giving me errors!
                //GetComponent<SlingSound>().played = false;         You are giving me errors!
				//GetComponent<SlingSound>().launch = true;          You are giving me errors!
            }

            if (fastendedTrigger)
			{
				OnFastenedDown();
				fastendedTrigger = false;
			}
		}
		else
        {
            sweatFX.Stop();
			if(slingCounter >= timeUntilSling)
			{
				Sling();
			}
			slingCounter = 0;

			if(!fastendedTrigger)
			{
				OnFastenedUp();
				fastendedTrigger = true;
			}
		}

		if (SumoInput.GetButtonDown(launch, transform))
		{
			foreach(Transform leg in legs)
			{
				if (leg.GetComponent<Slip>().legAnchor.GetComponent<Leg>().s1_side == false)
				{
                    if (leg.GetComponent<Slip>().legAnchor.GetComponent<Fall>().definitelyNotPartOut)
					{
						leg.GetComponent<Slip>().fastened = true;
					}
				}
			}
		}
        if (SumoInput.GetButtonDown(launchB, transform))
		{
			foreach(Transform leg in legs)
			{
				if (leg.GetComponent<Slip>().legAnchor.GetComponent<Leg>().s1_side)
				{
                    if (leg.GetComponent<Slip>().legAnchor.GetComponent<Fall>().definitelyNotPartOut)
					{
						leg.GetComponent<Slip>().fastened = true;
                    }
				}
			}
		}

		if (SumoInput.GetButtonUp(launch, transform))
		{
			foreach (Transform leg in legs)
			{
				if (!leg.GetComponent<Slip>().legAnchor.GetComponent<Leg>().s1_side)
				{
					leg.GetComponent<Slip>().fastened = false;
                }
			}
		}
		
		if (SumoInput.GetButtonUp(launchB, transform))
		{
			foreach (Transform leg in legs)
			{
				if (leg.GetComponent<Slip>().legAnchor.GetComponent<Leg>().s1_side)
				{
					leg.GetComponent<Slip>().fastened = false;
                }
			}
		}
		//Debug.Log(timeSinceSlingshotCancel);

		if (timeSinceSlingshotCancel <= DelayedSlingTime &&(SumoInput.GetButtonUp(launch, transform) || SumoInput.GetButtonUp(launchB, transform)) && matches == legs.Length)
		{
			//Sling();
		}

		timeSinceSlingshotCancel += Time.deltaTime;
	}

	/*void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().AddForce(-GetComponent<Rigidbody2D>().velocity * 20, ForceMode2D.Force);
	}*/
}