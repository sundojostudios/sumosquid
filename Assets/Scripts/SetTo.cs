﻿using UnityEngine;
using System.Collections;

public class SetTo : MonoBehaviour {

	[SerializeField] public Transform target;
    [SerializeField] public float speed = 0;

	void Update () {
        if (speed == 0)
        {
            transform.position = target.position;
        }
        else
        {
            if (transform != null && target != null)
            {
                transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * speed);
            }
        }
	}
}
