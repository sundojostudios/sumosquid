﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArenaDestroy : MonoBehaviour {

	[SerializeField] [Range(0, 60)] private float timeToStart;
	[SerializeField] List<Transform> parts;
	[SerializeField] [Range(0, 60)] private float shakeTime;
	[SerializeField] [Range(0, 60)] private float particleEffectTime;
	[SerializeField] [Range(0, 15)] private float collapseSpeed;
	private bool particlesActivated = false;
	private float shakeTimeCounter;
	private int iter = 0;
	private Vector3 orgPos;
	[SerializeField] private GameObject fenceRemovalFX;
    [SerializeField]
    private AudioSource collapseSound;
    private bool played = false;

    void Start()
	{
		iter = Random.Range(0, parts.Count);
		shakeTimeCounter = shakeTime;
		orgPos = parts[iter].position;
	}

	void Collapse()
	{
		if(shakeTimeCounter > 0)
		{
			parts[iter].transform.position = orgPos + new Vector3(Random.Range(-8, 8), Random.Range(-8, 8)) * Time.deltaTime;
			shakeTimeCounter -= Time.deltaTime;
            if(!played)
            {
                collapseSound.Play();
                played = true;
            }
			if (!particlesActivated && shakeTimeCounter <= particleEffectTime)
			{
				foreach(Transform tr in parts[iter].transform)
				{
                    Instantiate(fenceRemovalFX, tr.position, tr.rotation);
				}
				particlesActivated = true;
			}
		}
		else
		{
			parts[iter].transform.localScale -= Time.deltaTime * Vector3.one;
			if(parts[iter].transform.localScale.x <= 0)
			{
				shakeTimeCounter = shakeTime;
				particlesActivated = false;
				parts[iter].tag = "Arena Out";
				parts[iter].GetComponent<SpriteRenderer>().enabled = false;
				parts[iter].localScale = Vector3.one;
				parts.RemoveAt(iter);
                if(parts.Count == 0)
                {
                    return;
                }
				iter = Random.Range(0, parts.Count);
				orgPos = parts[iter].position;
                played = false;
			}
		}
	}

	void Update () {
		if(parts.Count == 0)
		{
			return;
		}

		if(timeToStart <= 0)
		{
			Collapse();
		}
		else
		{
			timeToStart -= Time.deltaTime;
		}
	}
}
