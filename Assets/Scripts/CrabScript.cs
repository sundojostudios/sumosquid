﻿using UnityEngine;
using System.Collections;


public class CrabScript : MonoBehaviour
{
    [SerializeField]
    private Transform[] positions;

    private Transform Targetposition;
    private bool isMoving = false;
    private bool isFalling = false;
    void Start ()
    {
        Targetposition = positions[1];
	}
	
	void Update ()
    {
        if (!isFalling)
        {
            if (Vector3.Distance(transform.position, Targetposition.position) < 0.3f)
            {
                transform.position = Targetposition.position;
            }
            if (transform.position != Targetposition.position)
            {
                isMoving = true;
                transform.position = Vector3.Lerp(transform.position, Targetposition.position, Time.deltaTime * 0.5f );
                GetComponent<Animator>().Play("Walk");
                transform.rotation = LookAt2D(transform.position, Targetposition.position);
            }
            else
            {
                isMoving = false;
                GetComponent<Animator>().Play("Idle");
            }
        }
        else
        {
            transform.localScale += new Vector3(-0.005f, -0.005f, 0);
            if (transform.localScale.x <= 0)
            {
                Destroy(gameObject);
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.gameObject.CompareTag("Player1") || other.gameObject.CompareTag("Player2")) && !isMoving)
        {
            while (Targetposition.position == transform.position)
            {
                int rand = Random.Range(0, positions.Length);
                Targetposition = positions[rand];
            }
        }
    }

    void OnColliderEnter2D(Collider2D other)
    {
        if (other.CompareTag("Arena Out"))
        {
            StartFall();
        }
    }

    public void StartFall()
    {
        isFalling = true;
    }
    Quaternion LookAt2D(Vector3 position, Vector3 targetPosition, int axis = 2)
    {
        Vector3 diff = targetPosition - position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        if (axis == 0)
        {
            return Quaternion.Euler(rot_z - 90, 0f, 0f);
        }
        else if (axis == 1)
        {
            return Quaternion.Euler(0f, rot_z - 90, 0f);
        }
        else
        {
            return Quaternion.Euler(0f, 0f, rot_z);
        }
    }
}
