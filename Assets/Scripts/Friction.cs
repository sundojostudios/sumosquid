﻿using UnityEngine;
using System.Collections;

public class Friction : MonoBehaviour {

    [SerializeField] private float a = 800;
    [SerializeField] private float b = 0.5f;
    [SerializeField] private float c = 10000;
    [SerializeField] private float d = 1.75f;
    [SerializeField] private float min = 15;
    [SerializeField] private float edgeDistance = 28;
	private float startFriction1;
	private float startFriction2;
	[SerializeField] private Transform bodyPlayer1;
    [SerializeField] private Transform bodyPlayer2;
    [SerializeField] private Transform thingyPlayer1;
    [SerializeField] private Transform thingyPlayer2;
	[SerializeField] private Transform center;

	/*void Start()
	{
		startFriction1 = bodyPlayer1.GetComponent<SpringJoint2D>().frequency;
		startFriction2 = bodyPlayer2.GetComponent<SpringJoint2D>().frequency;
	}
*/
	void Update()
	{
		if (bodyPlayer1 != null)
		{
            //thingyPlayer1.GetComponent<SetTo>().speed = edgeDistance + (1 / Vector3.Distance(bodyPlayer1.position, center.position)) * friction;
            thingyPlayer1.GetComponent<SetTo>().speed = Mathf.Clamp((c - Mathf.Lerp(Vector3.Distance(bodyPlayer1.position, center.position) * a, edgeDistance, b)) * d, min, c);
		}
		if (bodyPlayer2 != null)
		{
            //thingyPlayer2.GetComponent<SetTo>().speed = edgeDistance + (1 / Vector3.Distance(bodyPlayer2.position, center.position)) * friction;
            thingyPlayer2.GetComponent<SetTo>().speed = Mathf.Clamp((c - Mathf.Lerp(Vector3.Distance(bodyPlayer2.position, center.position) * a, edgeDistance, b)) * d, min, c);
		}
	}

	/*void OnTriggerStay2D(Collider2D coll)
	{
		if(bodyPlayer1 != null && coll.gameObject.layer == 8)
		{
			bodyPlayer1.GetComponent<SpringJoint2D>().frequency = friction;
		}
		else if(bodyPlayer2 != null && coll.gameObject.layer == 9)
		{
			bodyPlayer2.GetComponent<SpringJoint2D>().frequency = friction;
		}
	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if(bodyPlayer1 != null && coll.gameObject.layer == 8)
		{
			bodyPlayer1.GetComponent<SpringJoint2D>().frequency = startFriction1;
		}
		else if(bodyPlayer2 != null && coll.gameObject.layer == 9)
		{
			bodyPlayer2.GetComponent<SpringJoint2D>().frequency = startFriction2;
		}
	}*/
}
