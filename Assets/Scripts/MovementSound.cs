﻿using UnityEngine;
using System.Collections;

public class MovementSound : MonoBehaviour {

    public AudioSource source;
    public AudioClip movementClip;
    private float movementVolume = 4f;
	
	
	
	void Update ()
    {
	    if(true)
        {
            playMovementSound();
        }
	}
    
    void playMovementSound()
    {
        source.pitch = Random.Range(-1f, 1f);
        if(!source.isPlaying)
        { 
            source.PlayOneShot(movementClip, movementVolume);            
        }
    }
}
