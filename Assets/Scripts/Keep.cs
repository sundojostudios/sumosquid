﻿using UnityEngine;
using System.Collections;

public class Keep : MonoBehaviour
{
    [SerializeField] private bool music;

	void OnEnable ()
    {
        DontDestroyOnLoad(gameObject);

        Keep[] keeps = Object.FindObjectsOfType<Keep>();
        foreach (Keep keep in keeps)
        {
            if (keep.name == name && keep != this)
            {
                Destroy(gameObject);
            }
        }
	}
}
