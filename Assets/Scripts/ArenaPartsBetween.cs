﻿using UnityEngine;
using System.Collections;

public class ArenaPartsBetween : MonoBehaviour {

	[SerializeField] private Transform parentA;
	[SerializeField] private Transform parentB;

	void Update () {
		if (transform.parent != parentA && transform.parent != parentB)
		{
			if (parentA.localScale != Vector3.one)
			{
				transform.parent = parentB;
			}
			else if (parentB.localScale != Vector3.one)
			{
				transform.parent = parentA;
			}
		}

		if (parentA.GetComponent<SpriteRenderer>().enabled == false && parentB.GetComponent<SpriteRenderer>().enabled == false)
		{
			Destroy(gameObject);
		}
	}
}
