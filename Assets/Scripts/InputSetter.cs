﻿using UnityEngine;
using System.Collections;

public class InputSetter : MonoBehaviour {
	
	[SerializeField] private int currentMapping;

	[SerializeField] private SumoInput.KeyMap[] keyMaps;

	void Awake()
	{
		if (SumoInput.keyMapping.bindings == null)
		{
			SumoInput.keyMapping = keyMaps [currentMapping];
		}
	}
}
