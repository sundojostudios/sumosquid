﻿using UnityEngine;
using System.Collections;

public class Depth : MonoBehaviour {

    [System.Serializable] public struct Layer
    {
        public int depth;
        public Transform transform;
    }

    [SerializeField] private Transform center;
    [SerializeField] private Transform camera;
    [SerializeField] private Layer[] layers;

    void Update()
    {
        foreach(Layer layer in layers)
        {
            Vector2 pos = -(camera.position - center.position) * layer.depth * 0.1f;
            layer.transform.position = new Vector3(pos.x, pos.y, layer.transform.position.z);
        }
    }
}
