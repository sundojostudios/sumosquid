﻿using UnityEngine;
using System.Collections;

public class GlobalScale : MonoBehaviour {

	[SerializeField] public Vector3 scale = Vector3.one;
	[SerializeField] public Transform scaleTransform = null;

	void Update () {
		Transform parent = transform.parent;
		transform.parent = scaleTransform;
		transform.localScale = scale;
		transform.parent = parent;
	}

	public static void Scale(Transform transform, Transform scaleTransform = null, Vector3 scale = default(Vector3))
	{
		if(scale == Vector3.zero)
		{
			scale = Vector3.one;
		}
		Transform parent = transform.parent;
		transform.parent = scaleTransform;
		transform.localScale = scale;
		transform.parent = parent;
	}
}
