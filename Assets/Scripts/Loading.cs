﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public static class Loading
{
    public static void LoadScene(string name)
    {
        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            SceneManager.LoadScene(name);
            return;
        }

        if (SceneManager.sceneCount > 1)
        {
            return;
        }

        Transform loadingCamera = (new GameObject("Loading Camera")).transform;
        Camera.SetupCurrent(loadingCamera.gameObject.AddComponent<Camera>());
        loadingCamera.position = new Vector2(10000, 10000);
        SceneManager.LoadSceneAsync(name);
	}
}
