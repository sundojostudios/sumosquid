﻿using UnityEngine;
using System.Collections;

public class PathTransforming : MonoBehaviour
{
    [System.Serializable] public struct Trans
    {
        public Transform transform;
        [Range(0.1f, 10)] public float movementSpeed;
        [Range(0.1f, 10)] public float RotationSpeed;
        [Range(0.1f, 10)] public float ResizeSpeed;
        [Range(0.1f, 10)] public float pauseAfter;
		public bool lerp;
		public bool lookAt;
    }

    [SerializeField] private Trans[] transforms;
    private int iter = 0;
    private float orgSize;
    private float pauseCounter = 1;
    private bool pauseOn = false;

    void Start()
    {
        if(GetComponent<Camera>() != null)
        {
            orgSize = GetComponent<Camera>().orthographicSize;
        }

		if (GetComponent<LookAt> () == null)
		{
			gameObject.AddComponent<LookAt>();
		}
    }

    void Update()
    {
		if (transforms[iter].lerp)
		{
			transform.position = Vector3.Lerp(transform.position, transforms[iter].transform.position, transforms[iter].movementSpeed * Time.deltaTime);
			transform.rotation = Quaternion.Lerp(transform.rotation, transforms[iter].transform.rotation, transforms[iter].RotationSpeed * Time.deltaTime);
			transform.localScale = Vector3.Lerp(transform.localScale, transforms[iter].transform.localScale, transforms[iter].ResizeSpeed * Time.deltaTime);
		}
		else
		{
			transform.position = Vector3.MoveTowards(transform.position, transforms[iter].transform.position, transforms[iter].movementSpeed * Time.deltaTime);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, transforms[iter].transform.rotation, transforms[iter].RotationSpeed * Time.deltaTime);
			transform.localScale = Vector3.MoveTowards(transform.localScale, transforms[iter].transform.localScale, transforms[iter].ResizeSpeed * Time.deltaTime);
		}

		if (transforms [iter].lookAt)
		{
			GetComponent<LookAt>().target = transforms[iter].transform;
		}
		else
		{
			GetComponent<LookAt>().target = null;
		}
        
		if (GetComponent<Camera>() != null)
        {
            GetComponent<Camera>().orthographicSize = orgSize * transform.localScale.x;
        }
        if (Vector3.Distance(transform.position, transforms[iter].transform.position) < 0.5f)
        {
            if(!pauseOn)
            {
                pauseOn = true;
                pauseCounter = transforms[iter].pauseAfter;
            }
            pauseCounter -= Time.deltaTime;
            if (pauseCounter <= 0)
            {
                pauseOn = false;
                iter++;
                if (iter >= transforms.Length)
                {
                    iter = 0;
                }
            }
        }
       // if (transform.position.z <= )
    }


}

/*

    a = new Vector3(Mathf.sin(Mathf.Deg2Rad * legPartBAngle), Mathf.cos(Mathf.Deg2Rad * legPartBAngle), 0) * verticePositionB
    new Vector3(Mathf.sin(Mathf.Deg2Rad * legPartAAngle) * a.x, Mathf.cos(Mathf.Deg2Rad * legPartAAngle) * a.y, 0)

    

*/
