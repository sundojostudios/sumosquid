﻿using UnityEngine;
using System.Collections;

public class Windy : MonoBehaviour
{
    [SerializeField] Transform Player1Body;
    [SerializeField] Transform Player2Body;
    float randomRotation = 0;
    void OnEnable()
    {
        randomRotation = Random.Range(0, 360);
        Vector3 tempRotation = transform.rotation.eulerAngles;
        tempRotation = new Vector3(tempRotation.x, tempRotation.y, randomRotation);
        transform.rotation = Quaternion.Euler(tempRotation);
    }
	void Update ()
    {
        if (Player1Body != null && Player2Body != null)
        {
            Player1Body.GetComponent<Rigidbody2D>().AddForce(DegreeToVector2(randomRotation) * -250);
            Player2Body.GetComponent<Rigidbody2D>().AddForce(DegreeToVector2(randomRotation) * -250);
        }
    }

    public static Vector2 RadianToVector2(float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }

    public static Vector2 DegreeToVector2(float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }
}
