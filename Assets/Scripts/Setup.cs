﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Setup : MonoBehaviour {

	[SerializeField] private GameObject prefabBody;
	[SerializeField] private GameObject prefabStart;
	[SerializeField] private GameObject prefabMiddle;
	[SerializeField] private GameObject prefabEnd;
	[SerializeField] private GameObject prefabEndAnchor;

	[SerializeField] private Transform startPositionPlayer1;
	[SerializeField] private Transform startPositionPlayer2;

	[SerializeField] private int tentacleLength;
	[SerializeField] private int tentacleCount;

	[SerializeField] private int player1IsAI;
	[SerializeField] private int player2IsAI;

	private Transform body;
	private List<Transform> starts;
	private List<List<Transform>> middles;
	private List<Transform> ends;
	private List<Transform> endAnchors;
	private Transform player;

	void InstantiatePlayer(Transform player)
	{
		starts = new List<Transform>();
		middles = new List<List<Transform>>();
		ends = new List<Transform>();
		endAnchors = new List<Transform>();

		this.player = player;

		body = (Instantiate(prefabBody, player.localPosition, Quaternion.identity) as GameObject).transform;

		for (int i = 0; i < tentacleCount; i++)
		{
			Vector3 newPosition = player.localPosition + new Vector3(Mathf.Sin(1 / (i + 1)), Mathf.Cos(1 / (i + 1)), prefabStart.transform.localPosition.z);
			Transform start = (Instantiate(prefabStart, newPosition, Quaternion.identity) as GameObject).transform;
			starts.Add(start);
		}

		for (int iPart = 0; iPart < tentacleLength; iPart++)
		{
			middles.Add(new List<Transform>());
			for (int i = 0; i < tentacleCount; i++)
			{
				Vector3 newPosition = player.localPosition + new Vector3(Mathf.Sin(1 / (i + 1)) * iPart + 1, Mathf.Cos(1 / (i + 1)) * iPart + 1, prefabStart.transform.localPosition.z);
				Transform middle = (Instantiate(prefabMiddle, newPosition, Quaternion.identity) as GameObject).transform;
				middles[iPart].Add(middle);
			}
		}

		for (int i = 0; i < tentacleCount; i++)
		{
			Vector3 newPosition = player.localPosition + new Vector3(Mathf.Sin(1 / (i + 1)) * tentacleLength + 1, Mathf.Cos(1 / (i + 1)) * tentacleLength + 1, prefabStart.transform.localPosition.z);
			Transform end = (Instantiate(prefabEnd, newPosition, Quaternion.identity) as GameObject).transform;
			ends.Add(end);
		}

		for (int i = 0; i < tentacleCount; i++)
		{
			Vector3 newPosition = player.localPosition + new Vector3(Mathf.Sin(1 / (i + 1)) * 4, Mathf.Cos(1 / (i + 1)) * 4, prefabStart.transform.localPosition.z);
			Transform endAnchor = (Instantiate(prefabEndAnchor, newPosition, Quaternion.identity) as GameObject).transform;
			endAnchors.Add(endAnchor);
		}
	}

	void setupPlayer() //Call immedietly after InstantiatePlayer()
	{
		Controlls controlls = player.gameObject.GetComponent<Controlls>();
		controlls.selectLegAndLaunch = body.GetComponent<SelectLegAndLaunch>();
		controlls.inkSpray = body.GetComponent<InkSpray>();
		controlls.legs = new Leg[tentacleCount];
		for(int i = 0; i < tentacleCount; i++)
		{
			controlls.legs[i] = endAnchors[i].GetComponent<Leg>();
		}

		ColorPlayer colorPlayer = player.gameObject.GetComponent<ColorPlayer>();
		colorPlayer.slime = body.GetComponent<Slime>();
		

		body.transform.parent = player;
		body.GetComponent<SelectLegAndLaunch>().legs = ends.ToArray();

		SlipBody slipBody = body.GetComponent<SlipBody>();
		slipBody.legsEnd = new Slip[tentacleCount];
		for(int i = 0; i < tentacleCount; i++)
		{
			slipBody.legsEnd[i] = ends[i].GetComponent<Slip>();
		}

		body.GetComponent<Fall>().player = player;

		for(int i = 0; i < tentacleCount; i++)
		{
			starts[i].name = "Tentacle start " + i;
			starts[i].transform.parent = body.transform;
			starts[i].GetComponent<SpringJoint2D>().connectedBody = middles[0][i].GetComponent<Rigidbody2D>();
			starts[i].GetComponent<HingeJoint2D>().connectedBody = middles[0][i].GetComponent<Rigidbody2D>();
			starts[i].GetComponent<Line>().other = body;
			starts[i].GetComponent<DistanceJoint2D>().connectedBody = body.GetComponent<Rigidbody2D>();

			for (int iPart = 1; iPart < tentacleLength; iPart++)
			{
				middles[iPart][i].transform.parent = player;
				middles[iPart][i].GetComponent<Line>().other = middles[iPart - 1][i];
				middles[iPart][i].GetComponent<DistanceJoint2D>().connectedBody = middles[iPart - 1][i].GetComponent<Rigidbody2D>();
			}
			middles[0][i].GetComponent<Line>().other = starts[i];
			middles[0][i].GetComponent<DistanceJoint2D>().connectedBody = starts[i].GetComponent<Rigidbody2D>();
			for (int iPart = 0; iPart < tentacleLength - 1; iPart++)
			{
				middles[iPart][i].name = "Tentacle middle part " + iPart + " : " + i;
				middles[iPart][i].transform.parent = player;
				middles[iPart][i].GetComponent<SpringJoint2D>().connectedBody = middles[iPart + 1][i].GetComponent<Rigidbody2D>();
			}
			middles[tentacleLength - 1][i].name = "Tentacle middle part " + (tentacleLength - 1) + " : " + i;
			middles[tentacleLength - 1][i].transform.parent = player;
			middles[tentacleLength - 1][i].GetComponent<SpringJoint2D>().connectedBody = ends[i].GetComponent<Rigidbody2D>();
			middles[tentacleLength - 1][i].GetComponent<Line>().other = ends[i];
			middles[tentacleLength - 1][i].GetComponent<DistanceJoint2D>().distance = 0;

			ends[i].name = "Tentacle end " + i;
			ends[i].transform.parent = player;
			ends[i].GetComponent<SpringJoint2D>().connectedBody = endAnchors[i].GetComponent<Rigidbody2D>();
			ends[i].GetComponent<SpringJoint2D>().connectedBody = endAnchors[i].GetComponent<Rigidbody2D>();
			ends[i].GetComponent<Line>().other = middles[tentacleLength - 1][i];
			ends[i].GetComponent<DistanceJoint2D>().connectedBody = middles[tentacleLength - 1][i].GetComponent<Rigidbody2D>();
			ends[i].GetComponent<Slip>().body = body;
			ends[i].GetComponent<Slip>().legAnchor = endAnchors[i];

			endAnchors[i].name = "Tentacle end anchor " + i;
			endAnchors[i].transform.parent = player;
			endAnchors[i].GetComponent<Leg>().body = body;
			endAnchors[i].GetComponent<DistanceJoint2D>().connectedBody = ends[i].GetComponent<Rigidbody2D>();
			if(i >= tentacleCount / 2)
			{
				endAnchors[i].GetComponent<Leg>().s1_side = true;
			}
			else
			{
				endAnchors[i].GetComponent<Leg>().s1_side = false;
			}
		}
	}

	void Awake()
	{
		InstantiatePlayer(startPositionPlayer1);
		setupPlayer();
		InstantiatePlayer(startPositionPlayer2);
		setupPlayer();
	}
}
