﻿using UnityEngine;
using System.Collections;

public class PreserveData : MonoBehaviour
{
	[HideInInspector] static public int round = 0;
	[HideInInspector] static public int winsPlayer1 = 0;
	[HideInInspector] static public int winsPlayer2 = 0;
	[HideInInspector] static public float hue1A = 0;
	[HideInInspector] static public float hue1B = 0;
	[HideInInspector] static public float hue2A = 0;
	[HideInInspector] static public float hue2B = 0;
	[HideInInspector] static public bool warmup = true;
	[HideInInspector] static public bool player1AI = false;
	[HideInInspector] static public bool player2AI = false;
    [HideInInspector] static public bool inControl = false;
    [HideInInspector] static public RandomizeOctopi.OctopusStyle player1Style;
    [HideInInspector] static public RandomizeOctopi.OctopusStyle player2Style;
    [HideInInspector] static public bool removeMusic;
}
