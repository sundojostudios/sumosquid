﻿using UnityEngine;
using System.Collections;

public class FenceDestruction : MonoBehaviour {

	[SerializeField] private GameObject fx;
	[SerializeField] private float forceRequired = 12;
    [SerializeField] private AudioClip fenceImpact;
    [SerializeField] private AudioClip fenceDestroy;
    [SerializeField] private AudioSource source;
    private bool soundFXDestroy;
    private bool soundFXImpact;

	void Update()
	{
		if (!GetComponent<Rigidbody2D>().isKinematic)
		{
			transform.localScale -= new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime);

			if(transform.localScale.x <= 0)
			{
				Destroy(gameObject);
			}
		}
        if (soundFXDestroy)
        {
            PlaySoundDestroy();
        }
        if (soundFXImpact)
        {
            PlaySoundImpact();
        }
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (!GetComponent<Rigidbody2D>().isKinematic|| coll.gameObject.CompareTag("Fence"))
		{
			return;
		}
		if (coll.relativeVelocity.magnitude >= forceRequired)
		{
			GetComponent<Rigidbody2D> ().isKinematic = false;
			GetComponent<Rigidbody2D> ().drag = GetComponent<Rigidbody2D> ().angularDrag = 1;
			GetComponent<Rigidbody2D> ().AddRelativeForce(coll.relativeVelocity, ForceMode2D.Impulse);
			Instantiate(fx, transform.position, transform.rotation);
            soundFXDestroy = true;
            
		}
        else if(coll.relativeVelocity.magnitude > 1 && coll.relativeVelocity.magnitude < forceRequired)
        {
            soundFXImpact = true;
        }
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Arena Part")
		{
			GetComponent<Rigidbody2D> ().isKinematic = false;
			GetComponent<Rigidbody2D> ().drag = GetComponent<Rigidbody2D> ().angularDrag = 1;
			Instantiate (fx, transform.position, transform.rotation);
		}
	}

    void PlaySoundDestroy()
    {
        if(!source.isPlaying)
        {
            source.pitch = Random.Range(0.8f, 1.4f);
            source.PlayOneShot(fenceImpact, 0.7f);
            source.PlayOneShot(fenceDestroy, 0.7f);
            
        }
        soundFXDestroy = false;
    }

    void PlaySoundImpact()
    {
        if (!source.isPlaying)
        {
            source.pitch = Random.Range(0.5f, 1.4f);
            source.PlayOneShot(fenceImpact, 0.7f);

        }
        soundFXImpact = false;
    }
}
