﻿using UnityEngine;
using System.Collections;

public class StartPosition : MonoBehaviour {

    [HideInInspector] public bool pressed = false;

    void OnCollisionEnter2D(Collision2D coll)
    {
        SelectLegAndLaunch sllal = coll.gameObject.GetComponent<SelectLegAndLaunch>();
        if (sllal != null)
		{
            if (sllal.timeSinceSlingshot <= sllal.timeSinceSlingshotInputBlockLimit)
            {
                pressed = true;
                GetComponent<Rigidbody2D>().isKinematic = false;
                GetComponent<Rigidbody2D>().drag = GetComponent<Rigidbody2D> ().angularDrag = 1;
                GetComponent<Rigidbody2D>().AddRelativeForce(coll.relativeVelocity, ForceMode2D.Impulse);
            }
		}
    }

    void Update()
    {
        if (!GetComponent<Rigidbody2D>().isKinematic)
        {
            transform.localScale -= new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime);

            if(transform.localScale.x <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
