﻿using UnityEngine;
using System.Collections;

public class AIController : MonoBehaviour
{
	enum Action
	{
		DOWN,
		PRESS,
		UP,
		NONE
	}

    [SerializeField] public Transform otherPlayerBody;
	private Action action = Action.NONE;
	private float counter = 0;
	static private bool slingModeOn = false;

    public float GetAxis(string axisName)
	{
		if (slingModeOn)
        {
            if (axisName == "Select Horizontal" || axisName == "p2 Select Horizontal" || axisName == "Move Horizontal" || axisName == "p2 Move Horizontal")
            {
                if (otherPlayerBody.position.x > transform.position.x)
				{
					return action == Action.NONE ? -1 : 1;
                }
                else
                {
					return action == Action.NONE ? 1 : -1;
                }
            }
            else
            {
                if (otherPlayerBody.position.y > transform.position.y)
                {
					return action == Action.NONE ? -1 : 1;
                }
                else
                {
					return action == Action.NONE ? 1 : -1;
                }
            }
        }
        else
        {
            if (axisName == "Select Horizontal" || axisName == "p2 Select Horizontal" || axisName == "Move Horizontal" || axisName == "p2 Move Horizontal")
            {
                if (otherPlayerBody.position.x > transform.position.x)
                {
					return action == Action.NONE ? 1 : -1;
                }
                else
                {
					return action == Action.NONE ? -1 : 1;
                }
            }
            else
            {
                if (otherPlayerBody.position.y > transform.position.y)
                {
					return action == Action.NONE ? 1 : -1;
                }
                else
                {
					return action == Action.NONE ? -1 : 1;
                }
            }
        }
    }

    public bool GetButton(string buttonName)
	{
		if (action == Action.PRESS)
		{
			return true;
		}
		return false;
    }

    public bool GetButtonDown(string buttonName)
	{
		if (action == Action.DOWN)
		{
			return true;
		}
		return false;
    }

    public bool GetButtonUp(string buttonName)
	{
		if (action == Action.UP)
		{
			return true;
		}
		return false;
    }

	void Update()
	{
		counter -= Time.deltaTime;
		if (counter <= 0)
		{
			if (action == Action.DOWN)
			{
				action = Action.PRESS;
				if (slingModeOn)
				{
					counter += 1;
				}
				else
				{
					counter += 0.25f;
				}
			}
			else if (action == Action.PRESS)
			{
				action = Action.UP;
				counter += 0.1f;
			}
			else if (action == Action.UP)
			{
				action = Action.NONE;
				counter += 0.5f;
			}
			else if (action == Action.NONE)
			{
				slingModeOn = Random.Range(0, 2) == 1;
				action = Action.DOWN;
				counter += 0.1f;
			}
		}
	}
}
