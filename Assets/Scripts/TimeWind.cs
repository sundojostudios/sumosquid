﻿using UnityEngine;
using System.Collections;

public class TimeWind : MonoBehaviour
{
    float startTime;
    float targetTime;
    float currentTime;
    [SerializeField]
    Transform windObject;
    void Start()
    {
        targetTime = Random.Range(10, 30);
        startTime = Time.time;
    }
	void Update ()
    {
        currentTime = Time.time;
	    if(Time.time - startTime >= targetTime)
        {
            StartCoroutine(Turnoff(Random.Range(5, 10)));
            startTime = Time.time;
            targetTime = Random.Range(10, 30);
            windObject.gameObject.SetActive(true);
        }
	}
    IEnumerator Turnoff(float time)
    {
        yield return new WaitForSeconds(time);
        windObject.gameObject.SetActive(false);
    }
}
