﻿using UnityEngine;
using System.Collections;

public class NewWobbly : MonoBehaviour {

    private float rotation = 0;
    private float squish = 1;
    [SerializeField] private Transform transformSquishNRotate;
    [SerializeField] private Transform transformRotateBack;
    [SerializeField] private Transform ownBody;
    [SerializeField] private Transform otherBody;
    [SerializeField] private float offset = 45;

    void Update()
    {
        transformSquishNRotate.localScale = new Vector3(transformSquishNRotate.localScale.x, Mathf.Lerp(transformSquishNRotate.localScale.y, squish, Time.deltaTime), transformSquishNRotate.localScale.z);
        transformRotateBack.localEulerAngles = new Vector3(transformRotateBack.eulerAngles.x, transformRotateBack.eulerAngles.y, offset - transformSquishNRotate.localEulerAngles.z);
    }

    void OnTriggerStay2D(Collider2D other)
    {   
        if (!other.isTrigger)
        {
            transformSquishNRotate.GetComponent<LookAt>().target = other.transform;
            float speed = 0.001f;
            if (other.GetComponent<Rigidbody2D>() != null)
            {
                speed += (ownBody.GetComponent<Rigidbody2D>().velocity - -other.GetComponent<Rigidbody2D>().velocity).magnitude;
            }
            else if (other.CompareTag("Player2") || other.CompareTag("Player1"))
            {
                speed += (ownBody.GetComponent<Rigidbody2D>().velocity - -otherBody.GetComponent<Rigidbody2D>().velocity).magnitude;
            }
            else
            {
                speed += ownBody.GetComponent<Rigidbody2D>().velocity.magnitude;
            }

            if (speed < 1)
            {
                squish = 1;
            }
            else
            {
                squish = 1 - (1 / speed * 10);
                squish = Mathf.Clamp(squish, 0.5f, 1);
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!other.isTrigger)
        {
            squish = 1;
        }
    }
}
