﻿using UnityEngine;
using System.Collections;

public class TentaclePart : MonoBehaviour {

	[SerializeField] private Transform next;
	[SerializeField] private float length;
	[SerializeField] private float width;
	[SerializeField] private Transform player;
	[SerializeField] public float minWidth = 0.1f;
	[SerializeField] public float maxWidth = 10;
	[SerializeField] public float aaa;

	void LookAt2D(Transform target)
	{
		Vector3 diff = target.position - transform.position;
		diff.Normalize();

		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
	}

	void ScaleAt2D(Transform target)
	{
		Vector3 diff = target.position - transform.position;
		if(transform.parent.name == "Body" && transform.parent.parent.name == "Magical Thing")
		{
			transform.localScale = new Vector3(Mathf.Clamp((width / (diff.magnitude / 5)) * aaa, minWidth, maxWidth), diff.magnitude / length, transform.localScale.z);
		}
		else
		{
			transform.localScale = (new Vector3(Mathf.Clamp(width / (diff.magnitude / 5), minWidth, maxWidth), diff.magnitude / length, transform.localScale.z));
		}
		
	}

	void Update () {
		LookAt2D(next);
		//ScaleAt2D(next);
	}
}
