﻿using UnityEngine;
using System.Collections;

public class Leg : MonoBehaviour
{
	[SerializeField] public bool s1_side;
    

	private Vector2 pivot;
	[HideInInspector] public bool active;
    [HideInInspector] public bool movementSound;
	[SerializeField] public Transform thingy;
	[SerializeField] public Transform body;

	[HideInInspector] public string moveHorizontal;
	[HideInInspector] public string moveVertical;
	[HideInInspector] public string selectHorizontal;
	[HideInInspector] public string selectVertical;

	private float slingMoveHorizontal;
	private float slingMoveVertical;
	private float slingSelectHorizontal;
	private float slingSelectVertical;

	private float relmod = 0.5f;// * 0.6f;

	void Awake ()
	{
		pivot = transform.localPosition / 2;
		//pivot = Vector2.zero;
	}

	void Update ()
	{
		//Transform parent = transform.parent;
		//transform.parent = body;

		if (body.GetComponent<SelectLegAndLaunch>().timeSinceSlingshot < body.GetComponent<SelectLegAndLaunch>().timeSinceSlingshotInputBlockLimit)
		{
			if (active)
			{

				if (s1_side)
				{
					transform.localPosition = Vector2.Lerp(transform.localPosition, (Vector2)thingy.localPosition + new Vector2(pivot.x + slingMoveHorizontal * 2.75f, pivot.y + slingMoveVertical * 2.75f) / relmod, Time.deltaTime * 10);
				}
				else
				{
					transform.localPosition = Vector2.Lerp(transform.localPosition, (Vector2)thingy.localPosition + new Vector2(pivot.x + slingSelectHorizontal * 2.75f, pivot.y + slingSelectVertical * 2.75f) / relmod, Time.deltaTime * 10);
				}

			}
			else
			{
				transform.localScale = new Vector2(1, 1);

			}

			return;
		}

		slingMoveHorizontal = -SumoInput.GetAxis(moveHorizontal, transform);
		slingMoveVertical = -SumoInput.GetAxis(moveVertical, transform);
		slingSelectHorizontal = -SumoInput.GetAxis(selectHorizontal, transform);
		slingSelectVertical = -SumoInput.GetAxis(selectVertical, transform);

		if (active)
		{
			
			if (s1_side)
			{
				transform.localPosition = Vector2.Lerp(transform.localPosition, (Vector2)thingy.localPosition + new Vector2(pivot.x + SumoInput.GetAxis(moveHorizontal, transform) * 2.75f, pivot.y + SumoInput.GetAxis(moveVertical, transform) * 2.75f) / relmod, Time.deltaTime * 10);
            }
			else
			{
				transform.localPosition = Vector2.Lerp(transform.localPosition, (Vector2)thingy.localPosition + new Vector2(pivot.x + SumoInput.GetAxis(selectHorizontal, transform) * 2.75f, pivot.y + SumoInput.GetAxis(selectVertical, transform) * 2.75f) / relmod, Time.deltaTime * 10);
            }
            
		}
		else
		{
			transform.localScale = new Vector2(1, 1);
           
        }
		//transform.parent = parent;
	}
}
