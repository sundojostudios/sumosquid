﻿using UnityEngine;
using System.Collections;

public class Slime : MonoBehaviour
{
    [SerializeField] private GameObject blob;
    private float blobCountdown;
    [SerializeField] private float frequency;
    [SerializeField] private Sprite[] blueSlimes;
    [SerializeField] private Sprite[] orangeSlimes;
    [SerializeField] private Sprite[] pinkSlimes;
    [SerializeField] private Sprite[] greenSlimes;
    

    void Update()
    {
        if (blobCountdown >= 0)
        {
            blobCountdown -= Time.deltaTime;
        }
        else
        {
            GameObject blobInstance = (GameObject)Instantiate(blob, new Vector3(transform.position.x, transform.position.y, blob.transform.position.z), Quaternion.identity);
            blobInstance.GetComponent<Rigidbody2D>().AddForce(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), blob.transform.position.z), ForceMode2D.Impulse);
			if(CompareTag("Player1"))
            {
				if(PreserveData.player1Style.color == RandomizeOctopi.OctoColor.BLUE)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = blueSlimes[Random.Range(0, blueSlimes.Length)];
				}
				else if(PreserveData.player1Style.color == RandomizeOctopi.OctoColor.YELLOW)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = orangeSlimes[Random.Range(0, orangeSlimes.Length)];
				}
				else if(PreserveData.player1Style.color == RandomizeOctopi.OctoColor.PINK)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = pinkSlimes[Random.Range(0, pinkSlimes.Length)];
				}
				else if(PreserveData.player1Style.color == RandomizeOctopi.OctoColor.GREEN)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = greenSlimes[Random.Range(0, greenSlimes.Length)];
				}
			}
			else if(CompareTag("Player2"))
			{
				if(PreserveData.player2Style.color == RandomizeOctopi.OctoColor.BLUE)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = blueSlimes[Random.Range(0, blueSlimes.Length)];
				}
				else if(PreserveData.player2Style.color == RandomizeOctopi.OctoColor.YELLOW)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = orangeSlimes[Random.Range(0, orangeSlimes.Length)];
				}
				else if(PreserveData.player2Style.color == RandomizeOctopi.OctoColor.PINK)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = pinkSlimes[Random.Range(0, pinkSlimes.Length)];
				}
				else if(PreserveData.player2Style.color == RandomizeOctopi.OctoColor.GREEN)
				{
					blobInstance.GetComponent<SpriteRenderer>().sprite = greenSlimes[Random.Range(0, greenSlimes.Length)];
				}
			}

            blobInstance.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
            blobCountdown = frequency;
        }
    }
}
