﻿using UnityEngine;
using System.Collections;

public class BeGone : MonoBehaviour {

	[SerializeField] private float time = 0.3f;

	private float countDown;

	void Start()
	{
		countDown = time;
	}

	void Update () {
		if(countDown <= 0)
		{
			Destroy(gameObject);
		}
		countDown -= Time.deltaTime;
	}
}
