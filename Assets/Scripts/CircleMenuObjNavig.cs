﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CircleMenuObjNavig : MonoBehaviour
{
    [SerializeField] private string moveHorizontal;
    [SerializeField] private string moveVertical;
    [SerializeField] private string selectHorizontal;
    [SerializeField] private string selectVertical;

	[SerializeField] private Transform[] items;
    private Vector2 indexPos = Vector2.zero;
    private Vector2 lastInput = Vector2.zero;

    int GetAxisDown(string axis)
    {
        float inp = SumoInput.GetAxis(axis, transform);
        return Mathf.Abs(inp) <= 0.5f ? 0 : inp < 0 ? -1 : inp > 0 ? 1 : 0;
    }

	void Update()
    {
        Vector2 input = new Vector3(GetAxisDown(moveHorizontal) + GetAxisDown(selectHorizontal), GetAxisDown(moveVertical) + GetAxisDown(selectVertical));
        if(input == lastInput)
        {
            return;
        }
        foreach (Transform tr in items)
        {
            if (indexPos + input == tr.GetComponent<MenuOption>().inputDir)
            {
                indexPos += input;
                transform.position = tr.position;
            }
        }
        lastInput = input;
    }
}
