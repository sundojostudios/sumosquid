﻿using UnityEngine;
using System.Collections;

public class SwitchParent : MonoBehaviour {
    
	[SerializeField] private Transform parent;

	void Start () {
        transform.parent = parent;
	}
}
