﻿using UnityEngine;
using System.Collections;

public class PulsatingOpacity : MonoBehaviour {
	
	[SerializeField] [Range(0.1f, 10)] private float speed;
	[SerializeField] [Range(0, 1)] private float min;
	[SerializeField] [Range(0, 1)] private float max;

	void Update () {
		SpriteRenderer sprite = GetComponent<SpriteRenderer>();
		GetComponent<SpriteRenderer>().color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Sin(Time.time * speed) * ((min + max) / 2) + min);

	}
}
