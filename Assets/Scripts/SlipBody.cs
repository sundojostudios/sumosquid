﻿using UnityEngine;
using System.Collections;

public class SlipBody : MonoBehaviour { 

    [SerializeField] public Slip[] legsEnd;
    [SerializeField] public Transform body;
    [SerializeField] private GameObject impact;
    [SerializeField] private Transform inkInpact;
    [SerializeField]
    public AudioSource inkImpactSound;
    [SerializeField]
    public AudioSource impactSound;
    private bool disabling = false;

    void OnTriggerEnter2D(Collider2D other)
    {
		//if (other.CompareTag("DynamicParticle"))
  //      {
  //          foreach (Slip slip in legsEnd)
  //          {
  //              slip.fastened = false;
  //              slip.triggered = true;
  //              slip.legAnchor.GetComponent<Leg>().active = true;
  //          }
  //      }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.transform.GetComponent<Rigidbody2D>() == null)
        { 
            return;
        }
		if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.magnitude) < Mathf.Abs(coll.rigidbody.velocity.magnitude) && coll.relativeVelocity.magnitude >= 16 && !coll.gameObject.CompareTag("DynamicParticle") && !coll.gameObject.CompareTag("Fence"))
		{
            Instantiate(impact, new Vector3(transform.position.x, transform.position.y, impact.transform.position.z), Quaternion.identity);
            if(!impactSound.isPlaying)
            {
                impactSound.pitch = Random.Range(0.5f, 1.5f);
                impactSound.Play();
            }
            foreach (Slip slip in legsEnd)
            {
                slip.fastened = false;
                slip.triggered = true;
                slip.legAnchor.GetComponent<Leg>().active = true;
            }
            body.GetComponent<SelectLegAndLaunch>().jumpCoolDown = body.GetComponent<SelectLegAndLaunch>().jumpCoolDownMax;
            if (coll.transform.tag == "Player2" || coll.transform.tag == "Player1")
            {
                body.GetComponent<InkSpray>().filledInk += Mathf.Abs(coll.relativeVelocity.magnitude);
            }
            body.GetComponent<Rigidbody2D>().AddForce(-coll.relativeVelocity * 5, ForceMode2D.Impulse);
            coll.rigidbody.AddForce(coll.relativeVelocity * 5, ForceMode2D.Impulse);
            Camera.main.GetComponent<CameraController>().shakeAmount += coll.rigidbody.velocity;
        }

    }
    public void Release()
    {
        foreach (Slip slip in legsEnd)
        {
            slip.fastened = false;
            slip.triggered = true;
            slip.legAnchor.GetComponent<Leg>().active = true;
        }
    }
    public void PlayInk()
    {
        inkInpact.gameObject.SetActive(true);
        if (!inkImpactSound.isPlaying)
        {
            inkImpactSound.Play();
        }
        if(!disabling)
        {
            StartCoroutine(StopInk(0.1f));
        }
    }

    IEnumerator StopInk(float time)
    {
        int i = 0;
        disabling = true;
        while (i < 1)
        {

            yield return new WaitForSeconds(time);
        }
        inkInpact.gameObject.SetActive(false);
        disabling = false;
    }
}
