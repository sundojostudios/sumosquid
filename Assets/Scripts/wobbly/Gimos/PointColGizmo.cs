﻿using UnityEngine;
using System.Collections;

public class PointColGizmo : MonoBehaviour {

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }
}
