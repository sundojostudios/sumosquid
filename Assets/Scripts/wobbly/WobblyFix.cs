﻿using UnityEngine;
using System.Collections;

public class WobblyFix : MonoBehaviour
{
    private Transform refTransform;
    public Transform setRefTransform
    {
        set { refTransform = value; }
    }
    
    private bool xIsNegative;
    private bool yIsNegative;

    void Start ()
    {
        StartCoroutine(GetPointref(0.1f));

    }
	
	void Update ()
    {
        if (refTransform.position.x - transform.position.x < 0 && !xIsNegative)
        {
            transform.position = new Vector3(refTransform.position.x + 1, transform.position.y, transform.position.z);
        }
        if (refTransform.position.x - transform.position.x > 0 && xIsNegative)
        {
            transform.position = new Vector3(refTransform.position.x - 1, transform.position.y, transform.position.z);
        }
        if (refTransform.position.y - transform.position.y < 0 && !yIsNegative)
        {
            transform.position = new Vector3(transform.position.x , refTransform.position.y + 1, transform.position.z);
        }
        if (refTransform.position.y - transform.position.y > 0 && yIsNegative)
        {
            transform.position = new Vector3(transform.position.x, refTransform.position.y - 1, transform.position.z);
        }
    }

    IEnumerator GetPointref(float time)
    {
        yield return new WaitForSeconds(time);
        if (refTransform.position.x - transform.position.x < 0)
        {
            xIsNegative = true;
        }
        else
        {
            xIsNegative = false;
        }

        if (refTransform.position.y - transform.position.y < 0)
        {
            yIsNegative = true;
        }
        else
        {
            yIsNegative = false;
        }
    }
}
