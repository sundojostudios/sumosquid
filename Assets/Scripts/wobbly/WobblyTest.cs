﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]

public class WobblyTest : MonoBehaviour
{
	private Rigidbody2D myRigidbody = null;
	public List<GameObject> myList = new List<GameObject>();
    private List<Vector3> pointRefList = new List<Vector3>();
    private List<Vector3> pointRefAngeledList = new List<Vector3>();

    [SerializeField] private Slip[] legEnds;

    public GameObject bar;

    private bool refSet = false;
    private bool isDisabling = false;
    private int m_Springs = 8;
    private Quaternion refAngle;


    [SerializeField]
    private Transform player;
    [SerializeField]
    private AudioSource fallSound;
    [SerializeField]
    private AudioSource inkImpactSound;
    [SerializeField]
    private AudioSource impactSound;
    [SerializeField]
    private Transform otherPlayer;
    [SerializeField]
	[Range(0.5f, 2)]
	private float m_Length = 1;
	public float SpringLength
	{
		get { return m_Length; }
		set { m_Length = value; }
	}
	[SerializeField]
	[Range(1, 10)]
	private float m_Frequency = 6;
	public float SpringFrequency
	{
		get { return m_Frequency; }
		set { m_Frequency = value; }
	}
	[SerializeField]
	[Range(1, 100)]
	private float m_Mass;
	public float Mass
	{
		get { return m_Mass; }
		set { m_Mass = value; }
	}
	[SerializeField]
	[Range(0, 5)]
	private float m_Gravity;
	public float Gravity
	{
		get { return m_Gravity; }
		set { m_Gravity = value; }
	}
    [SerializeField]
    private Transform colliderParent;
    void Awake ()
	{
		myRigidbody = GetComponent<Rigidbody2D>();
		myRigidbody.mass = m_Mass;
		myRigidbody.gravityScale = m_Gravity;
		for (int i = 0; i < m_Springs; i++)
		{
			GameObject TempObj = Instantiate(bar) as GameObject;
			TempObj.transform.SetParent(transform);

			Rigidbody2D TempBod = TempObj.GetComponent<Rigidbody2D>();
			TempBod.mass = m_Mass;
			TempBod.gravityScale = m_Gravity;

			SpringJoint2D TempSpring = TempObj.GetComponent<SpringJoint2D>();
			TempSpring.frequency = m_Frequency;
			TempSpring.distance = m_Length;
			TempSpring.connectedBody = myRigidbody;

			WheelJoint2D TempWheelSpring = TempObj.AddComponent<WheelJoint2D>();
			TempWheelSpring.autoConfigureConnectedAnchor = false;
			JointSuspension2D TempSusp = TempWheelSpring.suspension;

			TempSusp.angle = (360 / m_Springs) * i + 90;
			TempWheelSpring.connectedBody = myRigidbody;
			TempWheelSpring.suspension = TempSusp;
            TempObj.GetComponent<Fall>().player = player;
            TempObj.GetComponent<Fall>().fallSound = fallSound;
            TempObj.GetComponent<SlipBody>().legsEnd = legEnds;
            TempObj.GetComponent<SlipBody>().body = transform;
            if (i >= m_Springs / 2 && i != 7)
			{
				if (i >= m_Springs / 4)
				{
					TempObj.transform.localPosition = new Vector3(-0.5f, 0.5f);
				}
				else
				{
					TempObj.transform.localPosition = new Vector3(0.5f, 0.5f);
				}
			}
            else if (i == 3)
            {
                if (i == 3)
                {
                    TempObj.transform.localPosition = new Vector3(0.5f, 0.5f);
                }
              
            }
			else
			{
				if (i >= m_Springs / 2 + m_Springs / 4)
				{
					TempObj.transform.localPosition = new Vector3(-0.5f, -0.5f);
				}
				else
				{
					TempObj.transform.localPosition = new Vector3(0.5f, -0.5f);
				}
			}
            TempObj.transform.parent = transform.parent;
            TempObj.GetComponent<WobblyFix>().setRefTransform = transform;
            TempObj.GetComponent<SlipBody>().impactSound = impactSound;
            TempObj.GetComponent<SlipBody>().inkImpactSound = inkImpactSound;
			myList.Add(TempObj);
		}
		for (int i = 0; i < m_Springs; i++)
		{

			if (i < m_Springs - 1)
			{
				SpringJoint2D TempSpring = myList[i].AddComponent<SpringJoint2D>();
				TempSpring.frequency = 1;
				TempSpring.connectedBody = myList[i + 1].GetComponent<Rigidbody2D>();
			}
			else
			{
				SpringJoint2D TempSpring = myList[i].AddComponent<SpringJoint2D>();
				TempSpring.frequency = 1;
				TempSpring.connectedBody = myList[0].GetComponent<Rigidbody2D>();
			}

		}
        StartCoroutine(GetPointref(0));
    }
	void Update()
	{
        myRigidbody.mass = m_Mass;
        myRigidbody.gravityScale = m_Gravity;
       
        
    }


    IEnumerator GetPointref(float time)
    {
        yield return new WaitForSeconds(time);
        refAngle = transform.localRotation;
        for (int i = 0; i < m_Springs; i++)
        {

            pointRefList.Add(myList[i].transform.localPosition);
            pointRefAngeledList.Add(myList[i].transform.localPosition);
        }
        refSet = true;
    }
    void FixedUpdate()
    {
        for (int i = 0; i < m_Springs; i++)
        {
            myList[i].GetComponents<SpringJoint2D>()[0].frequency = m_Frequency;
            myList[i].GetComponents<SpringJoint2D>()[0].distance = m_Length;
            myList[i].GetComponent<Rigidbody2D>().mass = m_Mass;
            myList[i].GetComponent<Rigidbody2D>().gravityScale = m_Gravity;

            WheelJoint2D TempWheelSpring = myList[i].GetComponent<WheelJoint2D>();
            JointSuspension2D TempSusp = TempWheelSpring.suspension;
            TempSusp.angle = (360 / m_Springs) * i + 90 + transform.localRotation.z;
            TempWheelSpring.suspension = TempSusp;

            if (refSet)
            {

                for (int r = 0; r < m_Springs; r++)
                {
                    float angle = Quaternion.Angle(transform.localRotation, refAngle);
                    pointRefAngeledList[r] = new Vector3(pointRefList[r].x + (m_Length * Mathf.Cos(angle * Mathf.Deg2Rad)), pointRefList[r].y + (m_Length * Mathf.Sin(angle * Mathf.Deg2Rad)));
                }
                if (myList[i].transform.position.x - transform.position.x > pointRefList[i].x + m_Length * 1.75f || myList[i].transform.position.x - transform.position.x < pointRefList[i].x - m_Length * 1.75f ||
                    myList[i].transform.position.y - transform.position.y > pointRefList[i].y + m_Length * 1.75f || myList[i].transform.position.y - transform.position.y < pointRefList[i].y - m_Length * 1.75f)
                {

                    myList[i].transform.position = transform.position + pointRefAngeledList[i];

                }
                if (otherPlayer != null)
                {
                    //if (Vector3.Distance(myList[i].transform.position, otherPlayer.position) < 1)
                    //{
                    //    myList[i].transform.position = transform.position + pointRefAngeledList[i];
                    //}
                }
            }
        }
    }

    //Draws visual aid to the editor
    void OnDrawGizmos()
	{
		Gizmos.color = Color.magenta;
		if (myList.Count > 0)
		{
            //draws the lines between the middle out
			for (int i = 0; i < m_Springs; i++)
			{
				Gizmos.DrawLine(transform.position, myList[i].GetComponent<SpringJoint2D>().transform.position);
			}
			for (int i = 0; i < m_Springs; i++)
			{
                Gizmos.color = Color.magenta;
				if (i < m_Springs - 1)
				{
					Gizmos.DrawLine(myList[i + 1].transform.position, myList[i].GetComponents<SpringJoint2D>()[1].transform.position);
				}
				else
				{
					Gizmos.DrawLine(myList[0].transform.position, myList[i].GetComponents<SpringJoint2D>()[1].transform.position);
				}
                Gizmos.color = Color.cyan;

                Gizmos.DrawWireSphere(myList[i].transform.position, myList[i].GetComponent<CircleCollider2D>().radius);

            }
        }
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, 0.5f);
	}

}
