﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour
{
    [SerializeField]
    Transform transformToFollow;
	
	void Update ()
    {
        transform.position = transformToFollow.position;
	}
}
