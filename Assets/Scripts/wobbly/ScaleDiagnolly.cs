﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScaleDiagnolly : MonoBehaviour
{
    public GameObject head = null;
    private WobblyTest headScript;
    private float referenceY;
    private float ReferenceX;
    private bool refSet = false;

    void Start()
    {
        headScript = head.GetComponent<WobblyTest>();
        StartCoroutine(ExecuteAfterTime(1));
    }


    void Update()
    {
        if (refSet)
        {
            if (((Vector2.Distance(headScript.myList[1].transform.position, headScript.myList[5].transform.position) / referenceY) * 1) > 0.8f && ((Vector2.Distance(headScript.myList[1].transform.position, headScript.myList[5].transform.position) / referenceY) * 1) < 1.1f)
            {
                transform.localScale = new Vector2(transform.localScale.x, (Vector2.Distance(headScript.myList[1].transform.position, headScript.myList[5].transform.position) / referenceY) * 1);
            }
            if (((Vector2.Distance(headScript.myList[3].transform.position, headScript.myList[7].transform.position) / ReferenceX) * 1) > 0.8f && ((Vector2.Distance(headScript.myList[3].transform.position, headScript.myList[7].transform.position) / ReferenceX) * 1) < 1.1f)
            { 
                transform.localScale = new Vector2(((Vector2.Distance(headScript.myList[3].transform.position, headScript.myList[7].transform.position) / ReferenceX) * 1), transform.localScale.y);
            }
        }
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        referenceY = Vector2.Distance(headScript.myList[1].transform.position, headScript.myList[5].transform.position);
        ReferenceX = Vector2.Distance(headScript.myList[3].transform.position, headScript.myList[7].transform.position);
        refSet = true;
    }
}
