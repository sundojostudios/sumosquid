﻿using UnityEngine;
using System.Collections;

public class InkRelease : MonoBehaviour
{
    Transform body;
    void Start()
    {
        body = transform.parent.GetChild(0);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
       
        //if (other.CompareTag("DynamicParticle"))
        //{
        //    body.gameObject.GetComponent<SlipBody>().Release();
        //}

    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("DynamicParticle"))
        {
            body.gameObject.GetComponent<SlipBody>().PlayInk();
        }
    }
}
