﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuOption : MonoBehaviour
{
    [SerializeField] MenuHandeler myHandeler;
    int myOptionNr;
	[SerializeField] public RandomizeOctopi randomizeOctopi;
    private bool[] isSelected = new bool[2];
    [SerializeField] private Sprite infoSprite;
    bool p1fx = false;
    bool p2fx = false;
    private bool lastPressPlayer1 = false;
    private bool lastPressPlayer2 = false;

    public void setOptionNr(int optionNr)
    {
        myOptionNr = optionNr;
    }
    [SerializeField] private bool disableMultiplePlayersChoose; 

    public Vector2 inputDir;

    void Update()
    {
        if (GetComponent<SpriteRenderer>().color == Color.red && !disableMultiplePlayersChoose)
        {
            return;
        }

        if ((SumoInput.GetButton("Launch", transform) || SumoInput.GetButton("Launch B", transform)) && isSelected[0] && !lastPressPlayer1)
        {
            myHandeler.PlayerSelect(1, myOptionNr);
        }
        if ((SumoInput.GetButton("p2 Launch", transform) || SumoInput.GetButton("p2 Launch B", transform)) && isSelected[1] && !lastPressPlayer2)
        {
            myHandeler.PlayerSelect(2, myOptionNr);
        }

        lastPressPlayer1 = SumoInput.GetButton("Launch", transform) || SumoInput.GetButton("Launch B", transform);
        lastPressPlayer2 = SumoInput.GetButton("p2 Launch", transform) || SumoInput.GetButton("p2 Launch B", transform);
    }

    
    void OnTriggerEnter2D(Collider2D other)
	{
        if (!myHandeler.pubSlot && other.CompareTag("Player1"))
        {
            isSelected[0] = true;
        }
        else if (!myHandeler.pubSlot &&other.CompareTag("Player2"))
        {
            isSelected[1] = true;
        }

        if (other.CompareTag ("Player1"))
		{
            if (randomizeOctopi == null)
            {
                p1fx = true;
            }
            else
            {
                if (name == "Green")
                {
                    PreserveData.player1Style.color = RandomizeOctopi.OctoColor.GREEN;
                }
                else if (name == "Pink")
                {
                    PreserveData.player1Style.color = RandomizeOctopi.OctoColor.PINK;
                }
                else if (name == "Blue")
                {
                    PreserveData.player1Style.color = RandomizeOctopi.OctoColor.BLUE;
                }
                else if (name == "Yellow")
                {
                    PreserveData.player1Style.color = RandomizeOctopi.OctoColor.YELLOW;
                }
                randomizeOctopi.RandomizePlayer1();
            }
		}
		else if (other.CompareTag ("Player2"))
        {
            if (randomizeOctopi == null)
            {
                p2fx = true;
            }
            else
            {
                if (name == "Green")
                {
                    PreserveData.player2Style.color = RandomizeOctopi.OctoColor.GREEN;
                }
                else if (name == "Pink")
                {
                    PreserveData.player2Style.color = RandomizeOctopi.OctoColor.PINK;
                }
                else if (name == "Blue")
                {
                    PreserveData.player2Style.color = RandomizeOctopi.OctoColor.BLUE;
                }
                else if (name == "Yellow")
                {
                    PreserveData.player2Style.color = RandomizeOctopi.OctoColor.YELLOW;
                }
                randomizeOctopi.RandomizePlayer2();
            }
		}
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player1") && myHandeler.infoPlayer1 != null)
        {
            if (randomizeOctopi == null)
            {
                if (myHandeler.infoPlayer1.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fadein") && p1fx)
                {
                    myHandeler.infoPlayer1.GetComponent<Animator>().Play("Fadeout");
                    p1fx = false;
                }
                if (myHandeler.infoPlayer1.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fadeout"))
                {
                    myHandeler.infoPlayer1.GetComponent<Animator>().Play("Fadein");
                    myHandeler.infoPlayer1.GetComponent<SpriteRenderer>().sprite = infoSprite;

                }
            }
        }
        else if (other.CompareTag("Player2") && myHandeler.infoPlayer2 != null)
        {
            if (randomizeOctopi == null)
            {

                if (myHandeler.infoPlayer2.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fadein") && p2fx)
                {
                    myHandeler.infoPlayer2.GetComponent<Animator>().Play("Fadeout");
                    p2fx = false;
                }
                if (myHandeler.infoPlayer2.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Fadeout") )
                {
                    myHandeler.infoPlayer2.GetComponent<Animator>().Play("Fadein");
                    myHandeler.infoPlayer2.GetComponent<SpriteRenderer>().sprite = infoSprite;

                }
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (!myHandeler.pubSlot && other.CompareTag("Player1"))
        {
            isSelected[0] = false;
        }
        if (!myHandeler.pubSlot && other.CompareTag("Player2"))
        {
            isSelected[1] = false;
        }
    }
    }
