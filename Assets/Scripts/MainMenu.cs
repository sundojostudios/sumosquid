﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	[SerializeField] private float volume;
	[Space(20)]
	[SerializeField] private Transform mainMenu;
	[SerializeField] private Transform arenaSelect;
	[SerializeField] private Transform settingsMenu;
	[SerializeField] private Transform creditsMenu;
	[SerializeField] private Transform customization;

	void StartPress()
	{
		mainMenu.gameObject.SetActive(false);
		arenaSelect.gameObject.SetActive(true);
		customization.gameObject.SetActive(false);
	}

	void StartSingleplayerPress()
	{
        GetComponent<RandomizeOctopi>().RandomizePlayer1();
        GetComponent<RandomizeOctopi>().RandomizePlayer2();
		PreserveData.player2AI = true;
		mainMenu.gameObject.SetActive(false);
		arenaSelect.gameObject.SetActive(false);
		customization.gameObject.SetActive(true);
	}

	void StartMultiplayerPress()
    {
        GetComponent<RandomizeOctopi>().RandomizePlayer1();
        GetComponent<RandomizeOctopi>().RandomizePlayer2();
		PreserveData.player2AI = false;
		mainMenu.gameObject.SetActive(false);
		arenaSelect.gameObject.SetActive(false);
		customization.gameObject.SetActive(true);
	}

	void SettingsPress()
	{
		mainMenu.gameObject.SetActive(false);
		settingsMenu.gameObject.SetActive(true);
		customization.gameObject.SetActive(false);
	}

	void CreditsPress()
	{
		mainMenu.gameObject.SetActive(false);
		creditsMenu.gameObject.SetActive(true);
		customization.gameObject.SetActive(false);
	}

	void QuitPress()
	{
		Application.Quit();
	}
	
	void lVolume()
	{
		AudioListener.volume -= 0.1f;
	}

	void hVolume()
	{
		AudioListener.volume += 0.1f;
	}

	void BackPress()
	{
		mainMenu.gameObject.SetActive(true);
		arenaSelect.gameObject.SetActive(false);
		settingsMenu.gameObject.SetActive(false);
		creditsMenu.gameObject.SetActive(false);
	}

	void BattleArena()
	{
		Loading.LoadScene("scene");
	}

	void Race()
	{
		Loading.LoadScene("Race");
	}

	void Soccer()
	{
		Loading.LoadScene("Soccer");
	}
}
