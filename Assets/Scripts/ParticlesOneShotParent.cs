﻿using UnityEngine;
using System.Collections;

public class ParticlesOneShotParent : MonoBehaviour {
 
	void Update () {
		if(GetComponent<ParticleSystem>().IsAlive() == false)
		{
            Destroy(transform.parent.gameObject);
		}
	}
}
