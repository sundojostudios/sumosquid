﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Recommendation : MonoBehaviour
{
    [SerializeField] private float fadeIn;
    [SerializeField] private float fadeOut;
    private bool fadedIn = false;
    private bool toFadeOut = false;

    void Start()
    {
        GetComponent<SpriteRenderer>().color = Color.clear;
    }

    void Update ()
    {
        if(toFadeOut)
        {
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, Mathf.Lerp(GetComponent<SpriteRenderer>().color.a, 0, Time.deltaTime * fadeOut));
            if (GetComponent<SpriteRenderer>().color.a <= 0.2f)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, Mathf.Lerp(GetComponent<SpriteRenderer>().color.a, 1, Time.deltaTime * fadeIn));
            if (GetComponent<SpriteRenderer>().color.a >= 0.8f)
            {
                fadedIn = true;
            }
        }

        if(fadedIn)
        {
            if (Input.anyKey)
            {
                toFadeOut = true;
            }
        }
	}
}
