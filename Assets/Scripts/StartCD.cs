﻿using UnityEngine;
using System.Collections;

public class StartCD : MonoBehaviour
{
    public bool play = false;
    private bool played = false;
    void Start()
    {
    }
	void Update ()
    {
        if (PreserveData.round == 0)
        {
            if (!played)
            {
                GetComponent<Animator>().Play("321");
                played = true;
            }
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Round1") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("321"))
            {
                play = true;
                PreserveData.inControl = true;
            }
        }
        else if (PreserveData.round == 1)
        {
            if (!played)
            {
                GetComponent<Animator>().Play("321");
                played = true;
            }
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Round2") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("321"))
            {
                play = true;
                PreserveData.inControl = true;
            }
        }
        else if (PreserveData.round == 2 && PreserveData.winsPlayer1 == PreserveData.winsPlayer2)
        {
            if (!played)
            {
                GetComponent<Animator>().Play("finalround");
                played = true;
            }
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("finalround") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("321"))
            {
                play = true;
                PreserveData.inControl = true;
            }
        }

        if (PreserveData.winsPlayer1 >= 1 && PreserveData.winsPlayer2 >= 1 && PreserveData.round >= 3)
        {
            if (!played)
            {
                GetComponent<Animator>().Play("Tiebreaker");
                played = true;
            }
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Tiebreaker") || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("321"))
            {
                play = true;
                PreserveData.inControl = true;
            }
        }

    }
}
