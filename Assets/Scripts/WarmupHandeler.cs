﻿using UnityEngine;
using System.Collections;

public class WarmupHandeler : MonoBehaviour {

    [SerializeField]
    private GameObject start1;
    [SerializeField]
    private GameObject start2;
    [SerializeField]
    private GameObject warmUp;
    [SerializeField]
    private GameObject Fence;
    private float warmupCountdown = 1f;
    private int warmupCountdownText;
    private bool startCountdown = false;
    private bool startPosition = true;
	[SerializeField] private ArenaDestroy arenaDestroy;

    void Start()
    {
        if (!PreserveData.warmup)
        {
            warmUp.SetActive(false);
        }
    }

    void Update ()
    {
        if (warmUp != null)
        {
			if (start1.GetComponent<StartPosition>().pressed && start2.GetComponent<StartPosition>().pressed && startPosition)
            {
                Fence.SetActive(true);
                startCountdown = true;
                startPosition = false;
                
            }
        }
        if (startCountdown == true)
        {
            StartCountdown();
        }
    }

  

    void StartCountdown()
    {
        warmupCountdown -= Time.deltaTime;
        warmupCountdownText = (int)warmupCountdown;
        if (warmupCountdownText == 0)
        {
            warmUp.SetActive(false);
            PreserveData.warmup = false;
            startCountdown = false;
			arenaDestroy.enabled = true;
        }
    }
}
