﻿using UnityEngine;
using System.Collections;

public class removeWithParent : MonoBehaviour {
	[SerializeField]Transform partner;

	void Update () {
		if (partner.gameObject == null || !partner.gameObject.GetComponent<SpriteRenderer>().enabled) {
			this.gameObject.SetActive (false);
		}
	}
}
