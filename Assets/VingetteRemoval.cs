﻿using UnityEngine;
using System.Collections;
namespace UnityStandardAssets.ImageEffects
{
    public class VingetteRemoval : MonoBehaviour
    {
        void Update()
        {
            if (PreserveData.inControl)
            {
                if (GetComponent<VignetteAndChromaticAberration>().intensity > 0)
                {
                    GetComponent<VignetteAndChromaticAberration>().intensity -= 0.01f;
                }
            }
        }
    }
}
